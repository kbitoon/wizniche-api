<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAreasDetailsTechniciansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_areas_details_technicians', function (Blueprint $table) {
            $table->bigInteger('service_areas_detail_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('service_areas_detail_id', 'sad_id_foreign')->references('id')->on('service_areas_details')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['service_areas_detail_id','user_id'], 'sad_id_user_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_areas_details_technicians');
    }
}
