<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateAccountsPropertiesTable.
 */
class CreateAccountsPropertiesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accounts_properties', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('address_id')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('mak')->nullable();
            $table->string('base_mak')->nullable();
            $table->string('address_key')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('formatted_apn')->nullable();
            $table->string('county')->nullable();
            $table->string('fips_code')->nullable();
            $table->string('census_track')->nullable();
            $table->string('census_block')->nullable();
            $table->string('owner_address')->nullable();
            $table->string('owner_city')->nullable();
            $table->string('owner_state')->nullable();
            $table->string('owner_zip')->nullable();
            $table->string('owner_mak')->nullable();
            $table->string('owner_type')->nullable();
            $table->string('owner_name_one_full')->nullable();
            $table->string('owner_name_two_full')->nullable();
            $table->decimal('estimated_value')->nullable()->default(0.0);
            $table->string('year_assessed')->nullable();
            $table->decimal('assessed_value_total')->nullable()->default(0.0);
            $table->decimal('assessed_value_land')->nullable()->default(0.0);
            $table->decimal('assessed_value_improvements')->nullable()->default(0.0);
            $table->decimal('tax_billed_amount')->nullable()->default(0.0);
            $table->string('year_built')->nullable();
            $table->string('zoned_code_local')->nullable();
            $table->decimal('area_building')->nullable()->default(0.0);
            $table->decimal('area_first_floor')->nullable()->default(0.0);
            $table->decimal('area_second_floor')->nullable()->default(0.0);
            $table->decimal('area_lot_acres')->nullable()->default(0.0);
            $table->decimal('area_lot_sf')->nullable()->default(0.0);
            $table->decimal('parking_garage_area')->nullable()->default(0.0);
            $table->integer('bedrooms_count')->nullable()->default(0);
            $table->integer('bath_count')->nullable()->default(0);
            $table->string('results')->nullable();

            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
		});
	}

    /**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accounts_properties');
	}
}
