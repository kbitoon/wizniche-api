<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class PaymentAcceptedCreditcardsTable.
 */
class CreatePaymentAcceptedCreditcardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_accepted_creditcards', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('payment_method_id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->boolean("is_enabled")->default(false);
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_accepted_creditcards');
	}
}
