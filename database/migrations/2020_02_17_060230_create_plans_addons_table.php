<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansAddonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans_addons', function (Blueprint $table) {
            $table->bigInteger('plan_id')->unsigned();
            $table->bigInteger('addon_id')->unsigned();
            $table->timestamp('added_at')->useCurrent();
            $table->timestamp('removed_at')->nullable();

            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('plan_id')->references('id')->on('plans')->onDelete('cascade');
            $table->foreign('addon_id')->references('id')->on('addons')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['plan_id','addon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans_addons');
    }
}
