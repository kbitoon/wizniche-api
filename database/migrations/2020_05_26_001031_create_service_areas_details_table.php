<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateServiceAreasDetailsTable.
 */
class CreateServiceAreasDetailsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_areas_details', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_area_id')->unsigned();
            $table->string('zone_name');
            $table->boolean("is_available_monday")->default(false);
            $table->boolean("is_available_tuesday")->default(false);
            $table->boolean("is_available_wednesday")->default(false);
            $table->boolean("is_available_thursday")->default(false);
            $table->boolean("is_available_friday")->default(false);
            $table->boolean("is_available_saturday")->default(false);
            $table->boolean("is_available_sunday")->default(false);

            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('service_area_id')->references('id')->on('service_areas')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_areas_details');
	}
}
