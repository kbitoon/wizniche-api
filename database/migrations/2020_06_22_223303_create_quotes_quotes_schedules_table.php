<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesQuotesSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes_quotes_schedules', function (Blueprint $table) {
            $table->bigInteger('quote_id')->unsigned();
            $table->bigInteger('quotes_schedule_id')->unsigned();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade');
            $table->foreign('quotes_schedule_id')->references('id')->on('quotes_schedules')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['quote_id','quotes_schedule_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes_quotes_schedules');
    }
}
