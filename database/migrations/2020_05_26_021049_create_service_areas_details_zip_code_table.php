<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceAreasDetailsZipCodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_areas_details_zip_codes', function (Blueprint $table) {
            $table->bigInteger('service_areas_detail_id')->unsigned();
            $table->bigInteger('zip_code_id')->unsigned();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('service_areas_detail_id')->references('id')->on('service_areas_details')->onDelete('cascade');
            $table->foreign('zip_code_id')->references('id')->on('zip_codes')->onDelete('cascade');

            //SETTING THE PRIMARY KEYS
            $table->primary(['service_areas_detail_id','zip_code_id'], 'sad_id_zp_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_areas_details_zip_codes');
    }
}
