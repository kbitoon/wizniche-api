<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class ServiceAreasTable.
 */
class CreateServiceAreasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('service_areas', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('address_id')->unsigned();
            $table->string('type')->default('zip');
            $table->double('mile_radius')->default(10);

            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('service_areas');
	}
}
