<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class BusinessHoursTable.
 */
class CreateBusinessHoursTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_hours', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('account_id')->unsigned();
            $table->integer('type')->unsigned();
            $table->string('monday_in')->nullable();
            $table->string('monday_out')->nullable();
            $table->string('tuesday_in')->nullable();
            $table->string('tuesday_out')->nullable();
            $table->string('wednesday_in')->nullable();
            $table->string('wednesday_out')->nullable();
            $table->string('thursday_in')->nullable();
            $table->string('thursday_out')->nullable();
            $table->string('friday_in')->nullable();
            $table->string('friday_out')->nullable();
            $table->string('saturday_in')->nullable();
            $table->string('saturday_out')->nullable();
            $table->string('sunday_in')->nullable();
            $table->string('sunday_out')->nullable();
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_hours');
	}
}
