<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateZipCodesTable.
 */
class CreateZipCodesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('zip_codes', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_area_id')->unsigned();
            $table->string('code');

            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('service_area_id')->references('id')->on('service_areas')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('zip_codes');
	}
}
