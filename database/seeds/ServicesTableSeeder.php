<?php

use App\Imports\ServicesImport;
use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new ServicesImport())->import(database_path('seeds/data/services.csv'));
    }
}
