<?php

use App\Imports\AddressesImport;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new AddressesImport())->import(database_path('seeds/data/addresses.csv'));
    }
}
