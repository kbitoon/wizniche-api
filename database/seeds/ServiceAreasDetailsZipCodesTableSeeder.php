<?php

use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use App\Models\ZipCode\ZipCode;
use Illuminate\Database\Seeder;

class ServiceAreasDetailsZipCodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zipCodes = ZipCode::all();
        $service_area_one = ServiceAreasDetail::find(1);
        $service_area_one->zipCodes()->sync($zipCodes);

        $service_area_two = ServiceAreasDetail::find(2);
        $service_area_two->zipCodes()->sync($zipCodes);
    }
}
