<?php

use App\Imports\AccountsNotificationsImport;
use Illuminate\Database\Seeder;

class AccountsNotificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new AccountsNotificationsImport())->import(database_path('seeds/data/accounts_notifications.csv'));
    }
}
