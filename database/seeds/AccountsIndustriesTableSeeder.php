<?php

use App\Models\Account\Account;
use Illuminate\Database\Seeder;

class AccountsIndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = Account::find(1);
        $administrator->industries()->sync([1]);

        $subscriber = Account::find(2);
        $subscriber->industries()->sync([1]);
    }
}
