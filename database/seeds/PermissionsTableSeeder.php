<?php

use App\Imports\PermissionsImport;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PermissionsImport())->import(database_path('seeds/data/permissions.csv'));
    }
}
