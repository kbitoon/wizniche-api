<?php

use App\Imports\AddonsImport;
use Illuminate\Database\Seeder;

class AddonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new AddonsImport())->import(database_path('seeds/data/addons.csv'));
    }
}
