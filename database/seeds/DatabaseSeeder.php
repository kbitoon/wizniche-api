<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RolesPermissionsTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(BusinessHoursTableSeeder::class);
        $this->call(CustomerTypesTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(AccountsServicesTaleSeeder::class);
        $this->call(AddressesTableSeeder::class);
        $this->call(AddonsTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(PlansAddonsTableSeeder::class);
        $this->call(AccountsPlansTableSeeder::class);
        $this->call(AccountsAddonsTableSeeder::class);
        $this->call(AccountsIndustriesTableSeeder::class);
        $this->call(AccountsNotificationsTableSeeder::class);
        $this->call(ServiceAreasTableSeeder::class);
        $this->call(ServiceAreasDetailsTableSeeder::class);
        $this->call(ZipCodeTableSeeder::class);
        $this->call(ServiceAreasDetailsZipCodesTableSeeder::class);
        $this->call(QuotesTableSeeder::class);
    }
}
