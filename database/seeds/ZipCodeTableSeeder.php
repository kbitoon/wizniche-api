<?php

use App\Imports\ZipCodeImport;
use Illuminate\Database\Seeder;

class ZipCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new ZipCodeImport())->import(database_path('seeds/data/zip_codes.csv'));
    }
}
