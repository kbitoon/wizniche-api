<?php

use App\Imports\ServiceAreaImport;
use Illuminate\Database\Seeder;

class ServiceAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new ServiceAreaImport())->import(database_path('seeds/data/service_areas.csv'));
    }
}
