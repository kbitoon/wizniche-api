<?php

use App\Imports\PlansImport;
use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new PlansImport())->import(database_path('seeds/data/plans.csv'));
    }
}
