<?php

use App\Imports\CustomerTypesImport;
use Illuminate\Database\Seeder;

class CustomerTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new CustomerTypesImport())->import(database_path('seeds/data/customer_types.csv'));
    }
}
