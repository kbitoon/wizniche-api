<?php

use App\Models\AccountsAddons\AccountsAddons;
use Illuminate\Database\Seeder;

class AccountsAddonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addons = [4,5];
        foreach ($addons as $addon) {
            AccountsAddons::create(['account_id' => 1, 'addon_id' => $addon]);
        }
    }
}
