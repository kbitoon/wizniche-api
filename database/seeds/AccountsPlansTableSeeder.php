<?php

use App\Models\AccountsPlans\AccountsPlans;
use Illuminate\Database\Seeder;

class AccountsPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AccountsPlans::create(['account_id' => 2, 'plan_id' => 1, 'max_users' => 1]);
    }
}
