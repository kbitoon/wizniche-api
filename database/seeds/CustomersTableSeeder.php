<?php

use App\Imports\CustomersImport;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new CustomersImport())->import(database_path('seeds/data/customers.csv'));
    }
}
