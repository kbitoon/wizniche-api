<?php

use App\Models\Account\Account;
use Illuminate\Database\Seeder;

class AccountsServicesTaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $account = Account::where('id', 1)->first();
        $account->services()->attach(1, ['price' => 5, 'cost' => 5]);

        $subscriber = Account::where('id', 2)->first();
        $subscriber->services()->attach(2, ['price' => 15, 'cost' => 15]);
    }
}
