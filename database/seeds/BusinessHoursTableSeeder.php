<?php

use App\Imports\BusinessHoursImport;
use Illuminate\Database\Seeder;

class BusinessHoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new BusinessHoursImport())->import(database_path('seeds/data/business_hours.csv'));
    }
}
