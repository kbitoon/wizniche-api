<?php

use App\Imports\RolesImport;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new RolesImport())->import(database_path('seeds/data/roles.csv'));
    }
}
