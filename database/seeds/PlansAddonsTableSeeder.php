<?php

use App\Models\Plan\Plan;
use Illuminate\Database\Seeder;

class PlansAddonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan = Plan::where('id', 1)->first();
        $plan->addons()->attach([1,2,3]);
    }
}
