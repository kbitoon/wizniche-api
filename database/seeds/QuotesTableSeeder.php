<?php

use App\Imports\QuotesImport;
use Illuminate\Database\Seeder;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new QuotesImport())->import(database_path('seeds/data/quotes.csv'));
    }
}
