<?php

use App\Imports\AccountsImport;
use Illuminate\Database\Seeder;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new AccountsImport())->import(database_path('seeds/data/accounts.csv'));
    }
}
