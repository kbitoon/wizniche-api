<?php

use App\Models\Permission\Permission;
use App\Models\Role\Role;
use App\Models\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrators = Role::where('slug',Role::ROLE_SUPER_ADMINISTRATOR)->first();
        $subscribers = Role::where('slug', Role::ROLE_SUBSCRIBER)->first();

        $administrator = new User();
        $administrator->account_id = 1;
        $administrator->first_name = 'Kheven';
        $administrator->last_name = 'Bitoon';
        $administrator->email = 'kbitoon@fullscale.io';
        $administrator->password = 'Passw0rd!';
        $administrator->email_verified_at = Carbon::now();
        $administrator->is_account_owner = 1;
        $administrator->save();
        $administrator->roles()->attach($administrators);

        $subscriber = new User();
        $subscriber->account_id = 2;
        $subscriber->first_name = 'Subscriber';
        $subscriber->last_name = 'Account';
        $subscriber->email = 'subscriber@mailinator.com';
        $subscriber->password = 'Passw0rd!';
        $subscriber->email_verified_at = Carbon::now();
        $subscriber->is_account_owner = 1;
        $subscriber->save();
        $subscriber->roles()->attach($subscribers);
    }
}
