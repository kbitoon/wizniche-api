<?php

use App\Imports\ServiceAreasDetailsImport;
use Illuminate\Database\Seeder;

class ServiceAreasDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new ServiceAreasDetailsImport())->import(database_path('seeds/data/service_areas_details.csv'));
    }
}
