<?php

use App\Models\Permission\Permission;
use App\Models\Role\Role;
use Illuminate\Database\Seeder;

class RolesPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscriber = Role::where('slug', 'subscriber')->first();
        $permissions = Permission::all();
        $subscriber->permissions()->sync($permissions);
    }
}
