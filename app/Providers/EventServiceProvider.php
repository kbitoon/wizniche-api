<?php

namespace App\Providers;

use App\Events\UserPasswordResetRequestEvent;
use App\Events\UserRegisteredEvent;
use App\Listeners\RunPostRegistrationListener;
use App\Listeners\SendPasswordResetListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegisteredEvent::class => [
            RunPostRegistrationListener::class
        ],
        UserPasswordResetRequestEvent::class => [
            SendPasswordResetListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
