<?php

namespace App\Providers;

use App\Repositories\Account\AccountRepository;
use App\Repositories\Account\AccountRepositoryEloquent;
use App\Repositories\AccountsAddons\AccountsAddonsRepository;
use App\Repositories\AccountsAddons\AccountsAddonsRepositoryEloquent;
use App\Repositories\AccountsNotifications\AccountsNotificationsRepository;
use App\Repositories\AccountsNotifications\AccountsNotificationsRepositoryEloquent;
use App\Repositories\AccountsPlans\AccountsPlansRepository;
use App\Repositories\AccountsPlans\AccountsPlansRepositoryEloquent;
use App\Repositories\Addon\AddonRepository;
use App\Repositories\Addon\AddonRepositoryEloquent;
use App\Repositories\Address\AddressRepository;
use App\Repositories\Address\AddressRepositoryEloquent;
use App\Repositories\BusinessHour\BusinessHourRepository;
use App\Repositories\BusinessHour\BusinessHourRepositoryEloquent;
use App\Repositories\Customer\CustomerRepository;
use App\Repositories\Customer\CustomerRepositoryEloquent;
use App\Repositories\CustomerType\CustomerTypeRepository;
use App\Repositories\CustomerType\CustomerTypeRepositoryEloquent;
use App\Repositories\EmployeesBusinessHour\EmployeesBusinessHourRepository;
use App\Repositories\EmployeesBusinessHour\EmployeesBusinessHourRepositoryEloquent;
use App\Repositories\EmployeesCertification\EmployeesCertificationRepository;
use App\Repositories\EmployeesCertification\EmployeesCertificationRepositoryEloquent;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Notification\NotificationRepositoryEloquent;
use App\Repositories\PasswordReset\PasswordResetRepository;
use App\Repositories\PasswordReset\PasswordResetRepositoryEloquent;
use App\Repositories\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardRepository;
use App\Repositories\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardRepositoryEloquent;
use App\Repositories\PaymentMethod\PaymentMethodRepository;
use App\Repositories\PaymentMethod\PaymentMethodRepositoryEloquent;
use App\Repositories\PaymentTransaction\PaymentTransactionRepository;
use App\Repositories\PaymentTransaction\PaymentTransactionRepositoryEloquent;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryEloquent;
use App\Repositories\Plan\PlanRepository;
use App\Repositories\Plan\PlanRepositoryEloquent;
use App\Repositories\Position\PositionRepository;
use App\Repositories\Position\PositionRepositoryEloquent;
use App\Repositories\Qoute\QuoteRepository;
use App\Repositories\Qoute\QuoteRepositoryEloquent;
use App\Repositories\QuotesSchedule\QuotesScheduleRepository;
use App\Repositories\QuotesSchedule\QuotesScheduleRepositoryEloquent;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryEloquent;
use App\Repositories\Service\ServiceRepository;
use App\Repositories\Service\ServiceRepositoryEloquent;
use App\Repositories\ServiceArea\ServiceAreaRepository;
use App\Repositories\ServiceArea\ServiceAreaRepositoryEloquent;
use App\Repositories\ServiceAreaDetail\ServiceAreasDetailRepository;
use App\Repositories\ServiceAreaDetail\ServiceAreasDetailRepositoryEloquent;
use App\Repositories\Setting\SettingRepository;
use App\Repositories\Setting\SettingRepositoryEloquent;
use App\Repositories\Tag\TagRepository;
use App\Repositories\Tag\TagRepositoryEloquent;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryEloquent;
use App\Repositories\ZipCode\ZipCodeRepository;
use App\Repositories\ZipCode\ZipCodeRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(RoleRepository::class, RoleRepositoryEloquent::class);
        $this->app->bind(PermissionRepository::class, PermissionRepositoryEloquent::class);
        $this->app->bind(AccountRepository::class, AccountRepositoryEloquent::class);
        $this->app->bind(CustomerRepository::class, CustomerRepositoryEloquent::class);
//        $this->app->bind(IndustryRepository::class, IndustryRepositoryEloquent::class);
        $this->app->bind(ServiceRepository::class, ServiceRepositoryEloquent::class);
        $this->app->bind(TagRepository::class, TagRepositoryEloquent::class);
//        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(AddressRepository::class, AddressRepositoryEloquent::class);
        $this->app->bind(PasswordResetRepository::class, PasswordResetRepositoryEloquent::class);
        $this->app->bind(CustomerTypeRepository::class, CustomerTypeRepositoryEloquent::class);
        $this->app->bind(PaymentTransactionRepository::class, PaymentTransactionRepositoryEloquent::class);
        $this->app->bind(PaymentMethodRepository::class, PaymentMethodRepositoryEloquent::class);
        $this->app->bind(PaymentAcceptedCreditcardRepository::class, PaymentAcceptedCreditcardRepositoryEloquent::class);
        $this->app->bind(NotificationRepository::class, NotificationRepositoryEloquent::class);
        $this->app->bind(BusinessHourRepository::class, BusinessHourRepositoryEloquent::class);
        $this->app->bind(ServiceAreaRepository::class, ServiceAreaRepositoryEloquent::class);
        $this->app->bind(SettingRepository::class, SettingRepositoryEloquent::class);
        $this->app->bind(AddonRepository::class, AddonRepositoryEloquent::class);
        $this->app->bind(PlanRepository::class, PlanRepositoryEloquent::class);
        $this->app->bind(AccountsAddonsRepository::class, AccountsAddonsRepositoryEloquent::class);
        $this->app->bind(AccountsPlansRepository::class, AccountsPlansRepositoryEloquent::class);
        $this->app->bind(AccountsNotificationsRepository::class, AccountsNotificationsRepositoryEloquent::class);
        $this->app->bind(ZipCodeRepository::class, ZipCodeRepositoryEloquent::class);
        $this->app->bind(ServiceAreasDetailRepository::class, ServiceAreasDetailRepositoryEloquent::class);
        $this->app->bind(EmployeesCertificationRepository::class, EmployeesCertificationRepositoryEloquent::class);
        $this->app->bind(EmployeesBusinessHourRepository::class, EmployeesBusinessHourRepositoryEloquent::class);
        $this->app->bind(PositionRepository::class, PositionRepositoryEloquent::class);
        $this->app->bind(QuoteRepository::class, QuoteRepositoryEloquent::class);
        $this->app->bind(QuotesScheduleRepository::class, QuotesScheduleRepositoryEloquent::class);
    }
}
