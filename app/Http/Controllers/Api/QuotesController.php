<?php

namespace App\Http\Controllers\Api;

use App\Criteria\RandomQuoteByStartEndDateCriteria;
use App\Transformers\Qoute\QuoteTransformer;
use Illuminate\Http\Request;
use App\Repositories\Qoute\QuoteRepository;
use App\Validators\Qoute\QuoteValidator;

/**
 * Class QuotesController.
 *
 * @package namespace App\Http\Controllers\Api\Qoute;
 */
class QuotesController extends ApiBaseController
{
    /**
     * QuotesController constructor.
     *
     * @param Request $request
     * @param QuoteRepository $repository
     * @param QuoteValidator $validator
     * @param QuoteTransformer $transformer
     */
    public function __construct(
        Request $request,
        QuoteRepository $repository,
        QuoteValidator $validator,
        QuoteTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-quote', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-quote', ['only' => ['store']]);
        $this->middleware('permission:edit-quote', ['only' => ['update']]);
        $this->middleware('permission:delete-quote', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function randomQuote()
    {
        $this->repository->pushCriteria(new RandomQuoteByStartEndDateCriteria());

        $with = $this->getEagerLoad();
        $limit = $this->request->get('limit', 1);

        $paginator = $this->repository->with($with)->inRandomOrder()->paginate($limit);

        if (! $paginator->count())
        {
            $this->repository->skipCriteria()->all();
            $paginator = $this->repository->with($with)->inRandomOrder()->paginate($limit);
        }
        return $this->respondWithCollection($paginator);
    }
}
