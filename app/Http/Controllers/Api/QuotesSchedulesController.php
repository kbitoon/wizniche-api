<?php

namespace App\Http\Controllers\Api;

use App\Repositories\QuotesSchedule\QuotesScheduleRepository;
use App\Transformers\QuotesSchedule\QuotesScheduleTransformer;
use App\Validators\QuotesSchedule\QuotesScheduleValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class QuotesSchedulesController.
 *
 * @package namespace App\Http\Controllers\Api\QuotesSchedule;
 */
class QuotesSchedulesController extends ApiBaseController
{
    /**
     * QuotesSchedulesController constructor.
     *
     * @param Request $request
     * @param QuotesScheduleRepository $repository
     * @param QuotesScheduleValidator $validator
     * @param QuotesScheduleTransformer $transformer
     */
    public function __construct(
        Request $request,
        QuotesScheduleRepository $repository,
        QuotesScheduleValidator $validator,
        QuotesScheduleTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-quote-schedule', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-quote-schedule', ['only' => ['store']]);
        $this->middleware('permission:edit-quote-schedule', ['only' => ['update']]);
        $this->middleware('permission:delete-quote-schedule', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Handle Quotes
            $this->handleQuotes($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Handle Quotes
            $this->handleQuotes($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param $entity
     */
    public function handleQuotes($entity)
    {
        if ($this->request->has('quotes')) {
            $quotes = $this->request->get('quotes');
            if ($quotes && !empty($quotes)) {
                $entity->quotes()->sync($quotes);
            } else {
                $entity->quotes()->sync([]);
            }
        }
    }
}
