<?php

namespace App\Http\Controllers\Api;

use App\Transformers\AccountsAddons\AccountsAddonsTransformer;
use App\Repositories\AccountsAddons\AccountsAddonsRepository;
use App\Validators\AccountsAddons\AccountsAddonsValidator;
use Illuminate\Http\Request;

/**
 * Class AccountsAddonsController.
 *
 * @package namespace App\Http\Controllers\Api\AccountsAddons;
 */
class AccountsAddonsController extends ApiBaseController
{
    /**
     * AccountsController constructor.
     *
     * @param Request $request
     * @param AccountsAddonsRepository $repository
     * @param AccountsAddonsValidator $validator
     * @param AccountsAddonsTransformer $transformer
     */
    public function __construct(
        Request $request,
        AccountsAddonsRepository $repository,
        AccountsAddonsValidator $validator,
        AccountsAddonsTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-account-addon', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-account-addon', ['only' => ['store']]);
        $this->middleware('permission:edit-account-addon', ['only' => ['update']]);
        $this->middleware('permission:delete-account-addon', ['only' => ['destroy']]);

        parent::__construct();
    }
}
