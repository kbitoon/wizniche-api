<?php

namespace App\Http\Controllers\Api;

use App\Traits\Tags\Tags;
use App\Transformers\Customer\CustomerTransformer;
use Dingo\Api\Http\Request;
use App\Repositories\Customer\CustomerRepository;
use App\Validators\Customer\CustomerValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


/**
 * Class CustomersController.
 *
 * @package namespace App\Http\Controllers\Api\Customer;
 */
class CustomersController extends ApiBaseController
{
    use Tags;

    /**
     * CustomersController constructor.
     *
     * @param Request $request
     * @param CustomerRepository $repository
     * @param CustomerValidator $validator
     * @param CustomerTransformer $transformer
     */
    public function __construct(
        Request $request,
        CustomerRepository $repository,
        CustomerValidator $validator,
        CustomerTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-customer', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-customer', ['only' => ['store']]);
        $this->middleware('permission:edit-customer', ['only' => ['update']]);
        $this->middleware('permission:delete-customer', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Handle Tags
            if ($this->request->has('tags')) {
                $this->handleTags($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Handle Tags
            if ($this->request->has('tags')) {
                $this->handleTags($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }
}
