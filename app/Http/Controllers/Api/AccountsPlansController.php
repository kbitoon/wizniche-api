<?php

namespace App\Http\Controllers\Api;

use App\Repositories\AccountsPlans\AccountsPlansRepository;
use App\Transformers\AccountsPlans\AccountsPlansTransformer;
use App\Validators\AccountsPlans\AccountsPlansValidator;
use Illuminate\Http\Request;

/**
 * Class AccountsPlansController.
 *
 * @package namespace App\Http\Controllers\Api\AccountsPlans;
 */
class AccountsPlansController extends ApiBaseController
{
    /**
     * AccountsPlansController constructor.
     *
     * @param Request $request
     * @param AccountsPlansRepository $repository
     * @param AccountsPlansValidator $validator
     * @param AccountsPlansTransformer $transformer
     */
    public function __construct(
        Request $request,
        AccountsPlansRepository $repository,
        AccountsPlansValidator $validator,
        AccountsPlansTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-account-plan', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-account-plan', ['only' => ['store']]);
        $this->middleware('permission:edit-account-plan', ['only' => ['update']]);
        $this->middleware('permission:delete-account-plan', ['only' => ['destroy']]);

        parent::__construct();
    }
}
