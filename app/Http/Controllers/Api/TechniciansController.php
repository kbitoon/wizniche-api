<?php

namespace App\Http\Controllers\Api;

use App\Criteria\TechniciansByRoleCriteria;
use App\Transformers\User\UserTransformer;
use App\Repositories\User\UserRepository;
use App\Validators\User\UserValidator;
use Dingo\Api\Http\Request;

/**
 * Class TechniciansController.
 *
 * @package namespace App\Http\Controllers\Api\User;
 */
class TechniciansController extends ApiBaseController
{
    /**
     * TechniciansController constructor.
     *
     * @param Request $request
     * @param UserRepository $repository
     * @param UserValidator $validator
     * @param UserTransformer $transformer
     */
    public function __construct(
        Request $request,
        UserRepository $repository,
        UserValidator $validator,
        UserTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-user', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-user', ['only' => ['store']]);
        $this->middleware('permission:edit-user', ['only' => ['update']]);
        $this->middleware('permission:delete-user', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(new TechniciansByRoleCriteria());
        return parent::index();
    }
}
