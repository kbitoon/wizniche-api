<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Position\PositionRepository;
use App\Transformers\Position\PositionTransformer;
use App\Validators\Position\PositionValidator;
use Illuminate\Http\Request;

/**
 * Class PositionsController.
 *
 * @package namespace App\Http\Controllers\Api\Position;
 */
class PositionsController extends ApiBaseController
{
    /**
     * PositionsController constructor.
     *
     * @param Request $request
     * @param PositionRepository $repository
     * @param PositionValidator $validator
     * @param PositionTransformer $transformer
     */
    public function __construct(
        Request $request,
        PositionRepository $repository,
        PositionValidator $validator,
        PositionTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-position', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-position', ['only' => ['store']]);
        $this->middleware('permission:edit-position', ['only' => ['update']]);
        $this->middleware('permission:delete-position', ['only' => ['destroy']]);

        parent::__construct();
    }
}
