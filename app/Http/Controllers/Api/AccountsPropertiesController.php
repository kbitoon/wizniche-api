<?php

namespace App\Http\Controllers\Api;

use App\Models\AccountsProperty\AccountsProperty;
use App\Services\MelissaDataApi;
use App\Transformers\AccountsProperty\AccountsPropertyTransformer;
use Illuminate\Http\Request;
use App\Validators\AccountsProperty\AccountsPropertyValidator;

/**
 * Class AccountsPropertiesController.
 *
 * @package namespace App\Http\Controllers\Api\AccountsProperty;
 */
class AccountsPropertiesController extends ApiBaseController
{
    protected $melissaDataApi;

    /**
     * AccountsPropertiesController constructor.
     *
     * @param Request $request
     * @param AccountsProperty $repository
     * @param AccountsPropertyValidator $validator
     * @param AccountsPropertyTransformer $transformer
     * @param MelissaDataApi $melissaDataApi
     */
    public function __construct(
        Request $request,
        AccountsProperty $repository,
        AccountsPropertyValidator $validator,
        AccountsPropertyTransformer $transformer,
        MelissaDataApi $melissaDataApi)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;
        $this->melissaDataApi = $melissaDataApi;

        $this->middleware('permission:view-account-property', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-account-property', ['only' => ['store']]);
        $this->middleware('permission:edit-account-property', ['only' => ['update']]);
        $this->middleware('permission:delete-account-property', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPropertyInformationByAddress()
    {
        $data = $this->request->all();
        if ($data) {
            $result = json_decode($this->melissaDataApi->getPropertyInformationByAddress($data));
            return $this->respondWithArray($result);
        }
        return null;
    }
}
