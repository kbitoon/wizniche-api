<?php

namespace App\Http\Controllers\Api;

use App\Repositories\EmployeesCertification\EmployeesCertificationRepository;
use App\Transformers\EmployeesCertification\EmployeesCertificationTransformer;
use App\Validators\EmployeesCertification\EmployeesCertificationValidator;
use Illuminate\Http\Request;

/**
 * Class EmployeesCertificationsController.
 *
 * @package namespace App\Http\Controllers\Api\EmployeesCertification;
 */
class EmployeesCertificationsController extends ApiBaseController
{
    /**
     * EmployeesCertificationsController constructor.
     *
     * @param Request $request
     * @param EmployeesCertificationRepository $repository
     * @param EmployeesCertificationValidator $validator
     * @param EmployeesCertificationTransformer $transformer
     */
    public function __construct(
        Request $request,
        EmployeesCertificationRepository $repository,
        EmployeesCertificationValidator $validator,
        EmployeesCertificationTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-employee-certification', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-employee-certification', ['only' => ['store']]);
        $this->middleware('permission:edit-employee-certification', ['only' => ['update']]);
        $this->middleware('permission:delete-employee-certification', ['only' => ['destroy']]);

        parent::__construct();
    }
}
