<?php

namespace App\Http\Controllers\Api;

use App\Criteria\ServicesByParentCriteria;
use App\Repositories\Service\ServiceRepository;
use App\Traits\Tags\Tags;
use App\Transformers\Service\ServiceTransformer;
use App\Validators\Service\ServiceValidator;
use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ServicesController.
 *
 * @package namespace App\Http\Controllers\Api\Service;
 */
class ServicesController extends ApiBaseController
{
    use Tags;

    /**
     * ServicesController constructor.
     *
     * @param Request $request
     * @param ServiceRepository $repository
     * @param ServiceValidator $validator
     * @param ServiceTransformer $transformer
     */
    public function __construct(
        Request $request,
        ServiceRepository $repository,
        ServiceValidator $validator,
        ServiceTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-service', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-service', ['only' => ['store']]);
        $this->middleware('permission:edit-service', ['only' => ['update']]);
        $this->middleware('permission:delete-service', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response
     */
    public function index()
    {
        $this->transformer->setDefaultIncludesByParams($this->request);
        return parent::index();
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response
     */
    public function show($id)
    {
        $this->transformer->setDefaultIncludesByParams($this->request);
        return parent::show($id);
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Handle Tags
            if ($this->request->has('tags')) {
                $this->handleTags($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Handle Tags
            if ($this->request->has('tags')) {
                $this->handleTags($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }
}
