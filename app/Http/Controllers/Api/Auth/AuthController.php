<?php

namespace App\Http\Controllers\Api\Auth;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Api\ApiBaseController;
use App\Http\Requests\Auth\LoginAuthRequest;
use App\Http\Requests\Auth\RegisterAuthRequest;
use App\Repositories\Account\AccountRepository;
use App\Repositories\User\UserRepository;
use App\Transformers\User\UserTransformer;
use App\Validators\User\UserValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends ApiBaseController
{
    const INVALID_TOKEN_ERRROR = 'This token is invalid.';

    /**
     * AuthController constructor.
     * @param Request $request
     * @param UserRepository $repository
     * @param UserValidator $validator
     * @param UserTransformer $transformer
     */
    public function __construct(
        Request $request,
        UserRepository $repository,
        UserValidator $validator,
        UserTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware( 'dbtransaction', [ 'only' => 'register' ] );
        parent::__construct();
    }

    /**
     * @param RegisterAuthRequest $request
     * @return mixed
     */
    public function register(RegisterAuthRequest $request)
    {
        $success['message'] = 'Thank you for signing up';
        event(new UserRegisteredEvent($request));
        return $this->respondWithArray($success);
    }

    /**
     * @param LoginAuthRequest $request
     * @return mixed
     */
    public function login(LoginAuthRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if(Auth::attempt($credentials)){
            $user = Auth::user();
            $success['token'] = $user->createToken('Personal Access Token')->accessToken;
            return $this->respondWithArray($success);
        } else {
            return $this->respondWithError('Unauthorised');
        }
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        Auth::user()->token()->revoke();
        return $this->respondWithArray([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function verify()
    {
        $user = $this->repository->where(DB::raw('md5(email)'), $this->request->route('id'))->first();
        if (! $user) {
            return $this->errorNotFound();
        }

        if (! $user->email_verified_at) {
            $user->markEmailAsVerified();
        }

        return $this->respondWithArray($user->toArray());
    }

    /**
     * @return mixed
     */
    public function authenticatedUser()
    {
        $user = Auth::user();
        $user->scope = $user->scope;
        $user->full_name = $user->full_name;

        return $this->respondWithArray(['user' => $user]);
    }
}
