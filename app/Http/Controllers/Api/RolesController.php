<?php

namespace App\Http\Controllers\Api;

use App\Criteria\RolesByAllowedRolesForEmployee;
use App\Transformers\Role\RoleTransformer;
use App\Repositories\Role\RoleRepository;
use App\Validators\Role\RoleValidator;
use Dingo\Api\Http\Request;

/**
 * Class RolesController.
 *
 * @package namespace App\Http\Controllers\Api\Role;
 */
class RolesController extends ApiBaseController
{
    /**
     * RolesController constructor.
     *
     * @param Request $request
     * @param RoleRepository $repository
     * @param RoleValidator $validator
     * @param RoleTransformer $transformer
     */
    public function __construct(
        Request $request,
        RoleRepository $repository,
        RoleValidator $validator,
        RoleTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-role', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-role', ['only' => ['store']]);
        $this->middleware('permission:edit-role', ['only' => ['update']]);
        $this->middleware('permission:delete-role', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response
     */
    public function getPublicRoles()
    {
        $this->repository->pushCriteria(new RolesByAllowedRolesForEmployee());
        return parent::index();
    }
}
