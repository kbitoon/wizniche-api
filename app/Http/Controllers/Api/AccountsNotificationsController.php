<?php

namespace App\Http\Controllers\Api;

use App\Repositories\AccountsNotifications\AccountsNotificationsRepositoryEloquent;
use App\Transformers\AccountsNotifications\AccountsNotificationsTransformer;
use App\Validators\AccountsNotifications\AccountsNotificationsValidator;
use Dingo\Api\Http\Request;

/**
 * Class AccountsNotificationsController.
 *
 * @package namespace App\Http\Controllers\Api\AccountsNotifications;
 */
class AccountsNotificationsController extends ApiBaseController
{
    /**
     * AccountsController constructor.
     *
     * @param Request $request
     * @param AccountsNotificationsRepositoryEloquent $repository
     * @param AccountsNotificationsValidator $validator
     * @param AccountsNotificationsTransformer $transformer
     */
    public function __construct(
        Request $request,
        AccountsNotificationsRepositoryEloquent $repository,
        AccountsNotificationsValidator $validator,
        AccountsNotificationsTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-account-notification', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-account-notification', ['only' => ['store']]);
        $this->middleware('permission:edit-account-notification', ['only' => ['update']]);
        $this->middleware('permission:delete-account-notification', ['only' => ['destroy']]);

        parent::__construct();
    }
}
