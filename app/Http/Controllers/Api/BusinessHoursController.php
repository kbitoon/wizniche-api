<?php

namespace App\Http\Controllers\Api;

use App\Repositories\BusinessHour\BusinessHourRepository;
use App\Transformers\BusinessHour\BusinessHourTransformer;
use App\Validators\BusinessHour\BusinessHourValidator;
use Illuminate\Http\Request;

/**
 * Class BusinessHoursController.
 *
 * @package namespace App\Http\Controllers\Api\BusinessHour;
 */
class BusinessHoursController extends ApiBaseController
{
    /**
     * BusinessHoursController constructor.
     *
     * @param Request $request
     * @param BusinessHourRepository $repository
     * @param BusinessHourValidator $validator
     * @param BusinessHourTransformer $transformer
     */
    public function __construct(
        Request $request,
        BusinessHourRepository $repository,
        BusinessHourValidator $validator,
        BusinessHourTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-business-hour', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-business-hour', ['only' => ['store']]);
        $this->middleware('permission:edit-business-hour', ['only' => ['update']]);
        $this->middleware('permission:delete-business-hour', ['only' => ['destroy']]);

        parent::__construct();
    }
}
