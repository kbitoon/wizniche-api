<?php

namespace App\Http\Controllers\Api;

use App\Repositories\EmployeesBusinessHour\EmployeesBusinessHourRepository;
use App\Transformers\EmployeesBusinessHour\EmployeesBusinessHourTransformer;
use App\Validators\EmployeesBusinessHour\EmployeesBusinessHourValidator;
use Illuminate\Http\Request;

/**
 * Class EmployeesBusinessHoursController.
 *
 * @package namespace App\Http\Controllers\Api\EmployeesBusinessHour;
 */
class EmployeesBusinessHoursController extends ApiBaseController
{
    /**
     * EmployeesBusinessHoursController constructor.
     *
     * @param Request $request
     * @param EmployeesBusinessHourRepository $repository
     * @param EmployeesBusinessHourValidator $validator
     * @param EmployeesBusinessHourTransformer $transformer
     */
    public function __construct(
        Request $request,
        EmployeesBusinessHourRepository $repository,
        EmployeesBusinessHourValidator $validator,
        EmployeesBusinessHourTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-employee-business-hour', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-employee-business-hour', ['only' => ['store']]);
        $this->middleware('permission:edit-employee-business-hour', ['only' => ['update']]);
        $this->middleware('permission:delete-employee-business-hour', ['only' => ['destroy']]);

        parent::__construct();
    }
}
