<?php

namespace App\Http\Controllers\Api;

use App\Transformers\CustomerType\CustomerTypeTransformer;
use App\Repositories\CustomerType\CustomerTypeRepository;
use App\Validators\CustomerType\CustomerTypeValidator;
use Illuminate\Http\Request;

/**
 * Class CustomerTypesController.
 *
 * @package namespace App\Http\Controllers\Api\CustomerType;
 */
class CustomerTypesController extends ApiBaseController
{
    /**
     * AddressesController constructor.
     *
     * @param Request $request
     * @param CustomerTypeRepository $repository
     * @param CustomerTypeValidator $validator
     * @param CustomerTypeTransformer $transformer
     */
    public function __construct(
        Request $request,
        CustomerTypeRepository $repository,
        CustomerTypeValidator $validator,
        CustomerTypeTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-customer-type', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-customer-type', ['only' => ['store']]);
        $this->middleware('permission:edit-customer-type', ['only' => ['update']]);
        $this->middleware('permission:delete-customer-type', ['only' => ['destroy']]);

        parent::__construct();
    }
}
