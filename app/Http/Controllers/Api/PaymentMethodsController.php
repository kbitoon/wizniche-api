<?php

namespace App\Http\Controllers\Api;

use App\Transformers\PaymentMethod\PaymentMethodTransformer;
use App\Repositories\PaymentMethod\PaymentMethodRepository;
use App\Validators\PaymentMethod\PaymentMethodValidator;
use Illuminate\Http\Request;

/**
 * Class PaymentMethodsController.
 *
 * @package namespace App\Http\Controllers\Api\PaymentMethod;
 */
class PaymentMethodsController extends ApiBaseController
{
    /**
     * PaymentMethodsController constructor.
     *
     * @param Request $request
     * @param PaymentMethodRepository $repository
     * @param PaymentMethodValidator $validator
     * @param PaymentMethodTransformer $transformer
     */
    public function __construct(
        Request $request,
        PaymentMethodRepository $repository,
        PaymentMethodValidator $validator,
        PaymentMethodTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-payment-method', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-payment-method', ['only' => ['store']]);
        $this->middleware('permission:edit-payment-method', ['only' => ['update']]);
        $this->middleware('permission:delete-payment-method', ['only' => ['destroy']]);

        parent::__construct();
    }
}
