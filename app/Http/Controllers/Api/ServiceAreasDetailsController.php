<?php

namespace App\Http\Controllers\Api;

use App\Transformers\ServiceAreaDetail\ServiceAreasDetailTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repositories\ServiceAreaDetail\ServiceAreasDetailRepository;
use App\Validators\ServiceAreaDetail\ServiceAreasDetailValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ServiceAreasDetailsController.
 *
 * @package namespace App\Http\Controllers\Api\ServiceAreaDetail;
 */
class ServiceAreasDetailsController extends ApiBaseController
{
    /**
     * AccountsController constructor.
     *
     * @param Request $request
     * @param ServiceAreasDetailRepository $repository
     * @param ServiceAreasDetailValidator $validator
     * @param ServiceAreasDetailTransformer $transformer
     */
    public function __construct(
        Request $request,
        ServiceAreasDetailRepository $repository,
        ServiceAreasDetailValidator$validator,
        ServiceAreasDetailTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-service-area-detail', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-service-area-detail', ['only' => ['store']]);
        $this->middleware('permission:edit-service-area-detail', ['only' => ['update']]);
        $this->middleware('permission:delete-service-area-detail', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Handle Technicians
            $this->handleTechnicians($item);

            // Handle ZipCodes
            $this->handleZipCodes($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Handle Technicians
            $this->handleTechnicians($item);

            // Handle ZipCodes
            $this->handleZipCodes($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param $entity
     */
    public function handleTechnicians($entity)
    {
        if ($this->request->has('technicians')) {
            $technicians = $this->request->get('technicians');
            if ($technicians && !empty($technicians)) {
                $entity->technicians()->sync($technicians);
            } else {
                $entity->technicians()->sync([]);
            }
        }
    }

    /**
     * This will handle the creation/updating of tags
     * @param $entity
     */
    public function handleZipCodes($entity)
    {
        if ($this->request->has('zip_codes')) {
            $zipCodes = $this->request->get('zip_codes');
            if ($zipCodes && !empty($zipCodes)) {
                $entity->zipCodes()->sync($zipCodes);
            } else {
                $entity->zipCodes()->sync([]);
            }
        }
    }
}
