<?php

namespace App\Http\Controllers\Api;

use App\Events\UserPasswordResetRequestEvent;
use App\Http\Requests\PasswordReset\PasswordResetCreateRequest;
use App\Http\Requests\PasswordReset\PasswordResetUpdateRequest;
use App\Notifications\PasswordResetSuccessNotification;
use App\Repositories\User\UserRepository;
use App\Repositories\PasswordReset\PasswordResetRepository;
use App\Transformers\PasswordReset\PasswordResetTransformer;
use App\Validators\PasswordReset\PasswordResetValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class PasswordResetsController.
 *
 * @package namespace App\Http\Controllers\Api\PasswordReset;
 */
class PasswordResetsController extends ApiBaseController
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * Invalid password token error message
     */
    const INVALID_PASSWORD_TOKEN_ERRROR = 'This password token is invalid.';

    /**
     * PasswordResetsController constructor.
     *
     * @param Request $request
     * @param PasswordResetRepository $repository
     * @param UserRepository $userRepository
     * @param PasswordResetValidator $validator
     * @param PasswordResetTransformer $transformer
     */
    public function __construct(
        Request $request,
        PasswordResetRepository $repository,
        UserRepository $userRepository,
        PasswordResetValidator $validator,
        PasswordResetTransformer $transformer)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-password-reset', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-password-reset', ['only' => ['store']]);
        $this->middleware('permission:edit-password-reset', ['only' => ['update']]);
        $this->middleware('permission:delete-password-reset', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * Will create a request to reset password
     *
     * @param PasswordResetCreateRequest $request
     * @return \Illuminate\Support\Facades\Response
     */
    public function create(PasswordResetCreateRequest $request)
    {
        $user = $this->userRepository->findByField('email', $request->email)->first();
        if (!$user) {
            return $this->errorNotFound();
        }
        event(new UserPasswordResetRequestEvent($user));
        return $this->respondWithArray([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($token)
    {
        $passwordReset = $this->repository->findByField('token', $token)->first();
        if (!$passwordReset) {
            return $this->respondWithError(self::INVALID_PASSWORD_TOKEN_ERRROR);
        }

        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return $this->respondWithError(self::INVALID_PASSWORD_TOKEN_ERRROR);
        }
        return $this->respondWithItem($passwordReset);
    }

    /**
     * @param PasswordResetUpdateRequest $request
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function reset(PasswordResetUpdateRequest $request)
    {
        $passwordReset = $this->repository->findWhere([
            'email' => $request->email,
            'token' => $request->token
        ])->first();
        if (!$passwordReset) {
            return $this->respondWithError(self::INVALID_PASSWORD_TOKEN_ERRROR);
        }

        $user = $this->userRepository->findByField('email', $request->email)->first();
        if (!$user) {
            return $this->errorNotFound();
        }

        $user->password = $request->password;
        $user->save();

        $passwordReset->delete();
        $user->notify(new PasswordResetSuccessNotification());
        return $this->respondWithArray($user->toArray());
    }
}
