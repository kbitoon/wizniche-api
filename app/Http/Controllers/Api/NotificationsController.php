<?php

namespace App\Http\Controllers\Api;

use App\Transformers\Notification\NotificationTransformer;
use App\Repositories\Notification\NotificationRepository;
use App\Validators\Notification\NotificationValidator;
use Illuminate\Http\Request;

/**
 * Class NotificationsController.
 *
 * @package namespace App\Http\Controllers\Api\Notification;
 */
class NotificationsController extends ApiBaseController
{
    /**
     * NotificationsController constructor.
     *
     * @param Request $request
     * @param NotificationRepository $repository
     * @param NotificationValidator $validator
     * @param NotificationTransformer $transformer
     */
    public function __construct(
        Request $request,
        NotificationRepository $repository,
        NotificationValidator $validator,
        NotificationTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-notification', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-notification', ['only' => ['store']]);
        $this->middleware('permission:edit-notification', ['only' => ['update']]);
        $this->middleware('permission:delete-notification', ['only' => ['destroy']]);

        parent::__construct();
    }
}
