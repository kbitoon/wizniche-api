<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Tag\TagRepository;
use App\Transformers\Tag\TagTransformer;
use App\Validators\Tag\TagValidator;
use Dingo\Api\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class TagsController.
 *
 * @package namespace App\Http\Controllers\Api\Tag;
 */
class TagsController extends ApiBaseController
{
    /**
     * TagsController constructor.
     *
     * @param Request $request
     * @param TagRepository $repository
     * @param TagValidator $validator
     * @param TagTransformer $transformer
     */
    public function __construct(
        Request $request,
        TagRepository $repository,
        TagValidator $validator,
        TagTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-tag', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-tag', ['only' => ['store']]);
        $this->middleware('permission:edit-tag', ['only' => ['update']]);
        $this->middleware('permission:delete-tag', ['only' => ['destroy']]);

        parent::__construct();
    }
}
