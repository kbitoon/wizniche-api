<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Permission\PermissionRepository;
use App\Transformers\Permission\PermissionTransformer;
use App\Validators\Permission\PermissionValidator;
use Dingo\Api\Http\Request;

/**
 * Class PermissionsController.
 *
 * @package namespace App\Http\Controllers\Api\Permission;
 */
class PermissionsController extends ApiBaseController
{
    /**
     * PermissionsController constructor.
     *
     * @param Request $request
     * @param PermissionRepository $repository
     * @param PermissionValidator $validator
     * @param PermissionTransformer $transformer
     */
    public function __construct(
        Request $request,
        PermissionRepository $repository,
        PermissionValidator $validator,
        PermissionTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-permission', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-permission', ['only' => ['store']]);
        $this->middleware('permission:edit-permission', ['only' => ['update']]);
        $this->middleware('permission:delete-permission', ['only' => ['destroy']]);

        parent::__construct();
    }
}
