<?php

namespace App\Http\Controllers\Api;

use App\Transformers\Account\AccountTransformer;
use App\Repositories\Account\AccountRepository;
use App\Validators\Account\AccountValidator;
use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class AccountsController.
 *
 * @package namespace App\Http\Controllers\Api\Account;
 */
class AccountsController extends ApiBaseController
{
    /**
     * AccountsController constructor.
     *
     * @param Request $request
     * @param AccountRepository $repository
     * @param AccountValidator $validator
     * @param AccountTransformer $transformer
     */
    public function __construct(
        Request $request,
        AccountRepository $repository,
        AccountValidator $validator,
        AccountTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-account', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-account', ['only' => ['store']]);
        $this->middleware('permission:edit-account', ['only' => ['update']]);
        $this->middleware('permission:delete-account', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // For account_industries table
            if ($this->request->has('industries')) {
                $industries = $this->request->get('industries');
                if ($industries && !empty(array_filter($industries))) {
                    $item->industries()->sync($industries);
                } else {
                    $item->industries()->sync([]);
                }
            }

            // For account_services table
            if ($this->request->has('services')) {
                $services = $this->request->get('services');
                if ($services && !empty(array_filter($services))) {
                    $item->services()->sync($services);
                } else {
                    $item->services()->sync([]);
                }
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }
}
