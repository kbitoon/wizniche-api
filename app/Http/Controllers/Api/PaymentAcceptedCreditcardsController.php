<?php

namespace App\Http\Controllers\Api;

use App\Transformers\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardTransformer;
use App\Repositories\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardRepository;
use App\Validators\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardValidator;
use Illuminate\Http\Request;

/**
 * Class PaymentAcceptedCreditcardsController.
 *
 * @package namespace App\Http\Controllers\Api\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcardsController extends ApiBaseController
{
    /**
     * PaymentAcceptedCreditcardsController constructor.
     *
     * @param Request $request
     * @param PaymentAcceptedCreditcardRepository $repository
     * @param PaymentAcceptedCreditcardValidator $validator
     * @param PaymentAcceptedCreditcardTransformer $transformer
     */
    public function __construct(
        Request $request,
        PaymentAcceptedCreditcardRepository $repository,
        PaymentAcceptedCreditcardValidator $validator,
        PaymentAcceptedCreditcardTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-payment-accepted-creditcard', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-payment-accepted-creditcard', ['only' => ['store']]);
        $this->middleware('permission:edit-payment-accepted-creditcard', ['only' => ['update']]);
        $this->middleware('permission:delete-payment-accepted-creditcard', ['only' => ['destroy']]);

        parent::__construct();
    }
}
