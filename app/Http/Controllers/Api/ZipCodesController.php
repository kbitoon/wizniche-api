<?php

namespace App\Http\Controllers\Api;

use App\Transformers\ZipCode\ZipCodeTransformer;
use Illuminate\Http\Request;
use App\Repositories\ZipCode\ZipCodeRepository;
use App\Validators\ZipCode\ZipCodeValidator;

/**
 * Class ZipCodesController.
 *
 * @package namespace App\Http\Controllers\Api\ZipCode;
 */
class ZipCodesController extends ApiBaseController
{
    /**
     * AccountsController constructor.
     *
     * @param Request $request
     * @param ZipCodeRepository $repository
     * @param ZipCodeValidator $validator
     * @param ZipCodeTransformer $transformer
     */
    public function __construct(
        Request $request,
        ZipCodeRepository $repository,
        ZipCodeValidator $validator,
        ZipCodeTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-zip-code', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-zip-code', ['only' => ['store']]);
        $this->middleware('permission:edit-zip-code', ['only' => ['update']]);
        $this->middleware('permission:delete-zip-code', ['only' => ['destroy']]);

        parent::__construct();
    }
}
