<?php

namespace App\Http\Controllers\Api;

use App\Transformers\Address\AddressTransformer;
use App\Repositories\Address\AddressRepository;
use App\Validators\Address\AddressValidator;
use Illuminate\Http\Request;

/**
 * Class AddressesController.
 *
 * @package namespace App\Http\Controllers\Api\Address;
 */
class AddressesController extends ApiBaseController
{
    /**
     * AddressesController constructor.
     *
     * @param Request $request
     * @param AddressRepository $repository
     * @param AddressValidator $validator
     * @param AddressTransformer $transformer
     */
    public function __construct(
        Request $request,
        AddressRepository $repository,
        AddressValidator $validator,
        AddressTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-address', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-address', ['only' => ['store']]);
        $this->middleware('permission:edit-address', ['only' => ['update']]);
        $this->middleware('permission:delete-address', ['only' => ['destroy']]);

        parent::__construct();
    }
}
