<?php

namespace App\Http\Controllers\Api;

use App\Transformers\Setting\SettingTransformer;
use App\Repositories\Setting\SettingRepository;
use App\Validators\Setting\SettingValidator;
use Illuminate\Http\Request;

/**
 * Class SettingsController.
 *
 * @package namespace App\Http\Controllers\Api\Setting;
 */
class SettingsController extends ApiBaseController
{
    /**
     * SettingsController constructor.
     *
     * @param Request $request
     * @param SettingRepository $repository
     * @param SettingValidator $validator
     * @param SettingTransformer $transformer
     */
    public function __construct(
        Request $request,
        SettingRepository $repository,
        SettingValidator $validator,
        SettingTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-setting', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-setting', ['only' => ['store']]);
        $this->middleware('permission:edit-setting', ['only' => ['update']]);
        $this->middleware('permission:delete-setting', ['only' => ['destroy']]);

        parent::__construct();
    }
}
