<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Plan\PlanRepository;
use App\Transformers\Plan\PlanTransformer;
use App\Validators\Plan\PlanValidator;
use Illuminate\Http\Request;

/**
 * Class PlansController.
 *
 * @package namespace App\Http\Controllers\Api\Plan;
 */
class PlansController extends ApiBaseController
{
    /**
     * PlansController constructor.
     *
     * @param Request $request
     * @param PlanRepository $repository
     * @param PlanValidator $validator
     * @param PlanTransformer $transformer
     */
    public function __construct(
        Request $request,
        PlanRepository $repository,
        PlanValidator $validator,
        PlanTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-plan', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-plan', ['only' => ['store']]);
        $this->middleware('permission:edit-plan', ['only' => ['update']]);
        $this->middleware('permission:delete-plan', ['only' => ['destroy']]);

        parent::__construct();
    }
}
