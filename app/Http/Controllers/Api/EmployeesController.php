<?php

namespace App\Http\Controllers\Api;

use App\Models\EmployeesCertification\EmployeesCertification;
use App\Models\Position\Position;
use App\Repositories\User\UserRepository;
use App\Traits\Tags\Tags;
use App\Transformers\User\UserTransformer;
use App\Validators\User\UserValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class EmployeesController.
 *
 * @package namespace App\Http\Controllers\Api\Employee;
 */
class EmployeesController extends ApiBaseController
{
    use Tags;

    /**
     * EmployeesController constructor.
     *
     * @param Request $request
     * @param UserRepository $repository
     * @param UserValidator $validator
     * @param UserTransformer $transformer
     */
    public function __construct(
        Request $request,
        UserRepository $repository,
        UserValidator $validator,
        UserTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-employee', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-employee', ['only' => ['store']]);
        $this->middleware('permission:edit-employee', ['only' => ['update']]);
        $this->middleware('permission:delete-employee', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Handle relationships if has any
            $this->handleRelationships($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Handle relationships if has any
            $this->handleRelationships($item);

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Handles all employee relationships
     *
     * @param $item
     */
    public function handleRelationships($item)
    {
        $this->handleTags($item);
        $this->handleRoles($item);
        $this->handleServices($item);
        $this->handlePositions($item);
    }

    /**
     * Hadnles the saving of roles
     *
     * @param $item
     */
    public function handleRoles($item)
    {
        if ($this->request->has('roles')) {
            $services = $this->request->get('roles');

            if ($services && !empty(array_filter($services))) {
                $item->roles()->sync($services);
            } else {
                $item->roles()->sync([]);
            }
        }
    }

    /**
     * Handles the saving of services
     *
     * @param $item
     */
    public function handleServices($item)
    {
        if ($this->request->has('services')) {
            $services = $this->request->get('services');

            if ($services && !empty(array_filter($services))) {
                $item->services()->sync($services);
            } else {
                $item->services()->sync([]);
            }
        }
    }

    /**
     * Hanldes the saving of positions
     *
     * @param $item
     */
    public function handlePositions($item)
    {
        if ($this->request->has('positions')) {
            $positions = $this->request->get('positions');

            if ($positions && !empty($positions)) {
                $item->positions()->sync($positions);
            } else {
                $item->positions()->sync([]);
            }
        }
    }
}
