<?php

namespace App\Http\Controllers\Api;

use App\Repositories\Addon\AddonRepository;
use App\Transformers\Addon\AddonTransformer;
use App\Validators\Addon\AddonValidator;
use Illuminate\Http\Request;

/**
 * Class AddonsController.
 *
 * @package namespace App\Http\Controllers\Api\Addon;
 */
class AddonsController extends ApiBaseController
{
    /**
     * AddonsController constructor.
     *
     * @param Request $request
     * @param AddonRepository $repository
     * @param AddonValidator $validator
     * @param AddonTransformer $transformer
     */
    public function __construct(
        Request $request,
        AddonRepository $repository,
        AddonValidator $validator,
        AddonTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-addon', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-addon', ['only' => ['store']]);
        $this->middleware('permission:edit-addon', ['only' => ['update']]);
        $this->middleware('permission:delete-addon', ['only' => ['destroy']]);

        parent::__construct();
    }
}
