<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Dingo\Api\Contract\Http\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class ApiBaseController extends Controller
{

    /**
     * HTTP header status code.
     *
     * @var int
     */
    protected $statusCode = 200;

    /**
     * Fractal Manager instance.
     *
     * @var Manager
     */
    protected $fractal;

    /**
     * Eloquent repository instance.
     *
     * @var \Illuminate\Database\Eloquent\Model;
     */
    protected $repository;

    /**
     * Fractal Transformer instance.
     *
     * @var \League\Fractal\TransformerAbstract
     */
    protected $transformer;

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * Illuminate\Http\Request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * Do we need to unguard the repository before create/update?
     *
     * @var bool
     */
    protected $unguard = false;

    /**
     * Number of items displayed at once if not specified.
     * There is no limit if it is 0 or false.
     *
     * @var int|bool
     */
    protected $defaultLimit = false;

    /**
     * Maximum limit that can be set via $_GET['limit'].
     *
     * @var int|bool
     */
    protected $maximumLimit = false;

    /**
     * Resource key for an item.
     *
     * @var string
     */
    protected $resourceKeySingular = 'data';

    /**
     * Resource key for a collection.
     *
     * @var string
     */
    protected $resourceKeyPlural = 'data';

    /**
     * ApiBaseController constructor.
     */
    public function __construct()
    {
        $this->fractal = new Manager();
        $this->fractal->setSerializer($this->serializer());

        if ($this->request->has('include')) {
            $this->fractal->parseIncludes(($this->request->input('include')));
        }

        $this->middleware('dbtransaction', ['only' => ['store', 'update', 'destroy']]);
    }

    /**
     * Serializer for the current repository.
     *
     * @return \League\Fractal\Serializer\SerializerAbstract
     */
    protected function serializer()
    {
        return new DataArraySerializer();
    }

    /**
     * Display a listing of the resource.
     * GET /api/{resource}.
     *
     * @return Response
     */
    public function index()
    {
        $with = $this->getEagerLoad();
        $limit = $this->calculateLimit();
        $loadAll = $this->request->input('all', false);

        if ($loadAll) {
            $paginator = $this->repository->with($with)->get();
        } else {
            $paginator = $this->repository->with($with)->paginate($limit);
        }

        return $this->respondWithCollection($paginator, $loadAll);
    }

    /**
     * Store a newly created resource in storage.
     * POST /api/{resource}.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());
            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Display the specified resource.
     * GET /api/{resource}/{id}.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        try {
            $with = $this->getEagerLoad();
            $item = $this->findItem($id, $with);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }
        return $this->respondWithItem($item);
    }

    /**
     * Update the specified resource in storage.
     * PUT /api/{resource}/{id}.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);
            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /api/{resource}/{id}.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $item = $this->findItem($id);
            $item->delete();
            return $this->respondWithArray(['message' => 'Deleted']);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }
    }

    /**
     * Getter for statusCode.
     *
     * @return int
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode.
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * Respond with a given item.
     *
     * @param $item
     *
     * @return mixed
     */
    protected function respondWithItem($item)
    {
        $resource = new Item($item, $this->transformer, $this->resourceKeySingular);
        $rootScope = $this->prepareRootScope($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Respond with a given collection.
     *
     * @param $collection
     * @param bool $loadAll
     * @return mixed
     */
    protected function respondWithCollection($collection, $loadAll=false)
    {
        if(! $loadAll) {
            $resource = new Collection($collection->items(), $this->transformer, $this->resourceKeyPlural);
            $resource->setPaginator(new IlluminatePaginatorAdapter($collection));
        } else {
            $resource = new Collection($collection, $this->transformer, $this->resourceKeyPlural);
        }

        $rootScope = $this->prepareRootScope($resource);
        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Respond with a given array of items.
     *
     * @param array $array
     * @param array $headers
     *
     * @return mixed
     */
    protected function respondWithArray(array $array, array $headers = [])
    {
        return response()->json($array, $this->statusCode, $headers);
    }

    /**
     * Response with the current error.
     *
     * @param string $message
     *
     * @return mixed
     */
    protected function respondWithError($message)
    {
        return $this->respondWithArray([
            'errors' => [
                'http_code' => $this->statusCode,
                'message'   => $message,
            ],
        ]);
    }

    /**
     * Prepare root scope and set some meta information.
     *
     * @param Item|Collection $resource
     *
     * @return \League\Fractal\Scope
     */
    protected function prepareRootScope($resource)
    {
        $resource->setMetaValue('available_includes', $this->transformer->getAvailableIncludes());
        $resource->setMetaValue('default_includes', $this->transformer->getDefaultIncludes());
        return $this->fractal->createData($resource);
    }

    /**
     * Generate a Response with a 404 HTTP header and a given message.
     *
     * @param string $message
     *
     * @return Response
     */
    protected function errorNotFound($message = 'Resource Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * Generate a Response with a 400 HTTP header and a given message.
     *
     * @param string$message
     *
     * @return Response
     */
    protected function errorWrongArgs($message = 'Wrong Arguments')
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * Specify relations for eager loading.
     *
     * @return array
     */
    protected function getEagerLoad()
    {
        $include = ($this->request->input('include', ''));
        $includes = explode(',', $include);
        $allowed = $this->transformer->getAvailableIncludes();
        $includes = array_filter(
            $includes,
            function ($val, $key) use ($allowed) {
                return isset($allowed[$key]) && (
                    $allowed[$key] === true || $allowed[$key] === $val
                );
            },
            ARRAY_FILTER_USE_BOTH
        );
        return $includes ?: [];
    }

    /**
     * Get item according to mode.
     *
     * @param int   $id
     * @param array $with
     *
     * @return mixed
     */
    protected function findItem($id, array $with = [])
    {
        if ($this->request->has('use_as_id')) {
            return $this->repository->with($with)->where($this->request->input('use_as_id'), '=', $id)->first();
        }
        return $this->repository->with($with)->find($id);
    }

    /**
     * Unguard eloquent repository if needed.
     */
    protected function unguardIfNeeded()
    {
        if ($this->unguard) {
            $this->repository->unguard();
        }
    }

    /**
     * Calculates limit for a number of items displayed in list.
     *
     * @return int
     */
    protected function calculateLimit()
    {
        $limit = (int) $this->request->input('limit', $this->defaultLimit);
        return ($this->maximumLimit && $this->maximumLimit < $limit) ? $this->maximumLimit : $limit;
    }
}
