<?php

namespace App\Http\Controllers\Api;

use App\Models\ZipCode\ZipCode;
use App\Services\ZipCodeApi;
use App\Transformers\ServiceArea\ServiceAreaTransformer;
use App\Repositories\ServiceArea\ServiceAreaRepository;
use App\Validators\ServiceArea\ServiceAreaValidator;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ServiceAreasController.
 *
 * @package namespace App\Http\Controllers\Api\ServiceArea;
 */
class ServiceAreasController extends ApiBaseController
{
    /**
     * @var ZipCodeApi
     */
    protected $zipCodeApi;

    /**
     * ServiceAreasController constructor.
     *
     * @param Request $request
     * @param ServiceAreaRepository $repository
     * @param ServiceAreaValidator $validator
     * @param ServiceAreaTransformer $transformer
     * @param ZipCodeApi $zipCodeApi
     */
    public function __construct(
        Request $request,
        ServiceAreaRepository $repository,
        ServiceAreaValidator $validator,
        ServiceAreaTransformer $transformer,
        ZipCodeApi $zipCodeApi)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->transformer = $transformer;
        $this->request = $request;
        $this->zipCodeApi = $zipCodeApi;

        $this->middleware('permission:view-service-area', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-service-area', ['only' => ['store']]);
        $this->middleware('permission:edit-service-area', ['only' => ['update']]);
        $this->middleware('permission:delete-service-area', ['only' => ['destroy']]);

        parent::__construct();
    }

    /**
     * @return \Illuminate\Support\Facades\Response|mixed
     * @throws GuzzleException
     */
    public function store()
    {
        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $this->unguardIfNeeded();
            $item = $this->repository->create($this->request->all());

            // Get all zip codes here
            if ($this->request->has('mile_radius')) {
                $this->handleZipCodesByRadius($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Support\Facades\Response|mixed
     * @throws GuzzleException
     */
    public function update($id)
    {
        try {
            $item = $this->findItem($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        try {
            $this->validator->with($this->request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $this->unguardIfNeeded();
            $item = $this->repository->update($this->request->all(), $item->id);

            // Get all zip codes here
            if ($this->request->has('mile_radius')) {
                $this->handleZipCodesByRadius($item);
            }

            return $this->respondWithItem($item);
        } catch (ValidatorException $e) {
            return $this->errorWrongArgs($e->getMessageBag()->getMessages());
        }
    }

    /**
     * Handles the saving of zip codes within mile radius
     *
     * @param $item
     * @throws GuzzleException
     */
    public function handleZipCodesByRadius($item)
    {
        if ($this->request->has('zip_code')) {
            $zip = $this->request->get('zip_code');
            $result = json_decode($this->zipCodeApi->getZipCodesByRadius($zip));

            $item->zipCodes()->delete();

            // Lets save the zip codes
            foreach ($result->zip_codes as $zipCode) {
                ZipCode::firstOrCreate(
                    [
                        'service_area_id' => $item->id,
                        'code' => $zipCode->zip_code
                    ]
                );
            }
        }
    }
}
