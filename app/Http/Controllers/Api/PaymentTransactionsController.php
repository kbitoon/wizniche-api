<?php

namespace App\Http\Controllers\Api;

use App\Models\PaymentTransaction\PaymentTransaction;
use App\Transformers\PaymentTransaction\PaymentTransactionTransformer;
use App\Validators\PaymentTransaction\PaymentTransactionValidator;
use Illuminate\Http\Request;

/**
 * Class PaymentTransactionsController.
 *
 * @package namespace App\Http\Controllers\Api\PaymentTransaction;
 */
class PaymentTransactionsController extends ApiBaseController
{
    /**
     * PaymentTransactionsController constructor.
     *
     * @param Request $request
     * @param PaymentTransaction $repository
     * @param PaymentTransactionValidator $validator
     * @param PaymentTransactionTransformer $transformer
     */
    public function __construct(
        Request $request,
        PaymentTransaction $repository,
        PaymentTransactionValidator $validator,
        PaymentTransactionTransformer $transformer)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->transformer = $transformer;
        $this->request = $request;

        $this->middleware('permission:view-payment-transaction', ['only' => ['index', 'show']]);
        $this->middleware('permission:create-payment-transaction', ['only' => ['store']]);
        $this->middleware('permission:edit-payment-transaction', ['only' => ['update']]);
        $this->middleware('permission:delete-payment-transaction', ['only' => ['destroy']]);

        parent::__construct();
    }
}
