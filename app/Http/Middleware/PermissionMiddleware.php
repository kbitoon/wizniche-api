<?php

namespace App\Http\Middleware;

use App\Models\Role\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param array $permissions
     * @return mixed
     */
    public function handle($request, Closure $next, ... $permissions)
    {
        $user =  Auth::user();
        if(is_null($user)){
            abort(404);
        }

        if($user->hasRole(Role::ROLE_SUPER_ADMINISTRATOR)) {
            return $next($request);
        }

        foreach($permissions as $permission){
            if ($user->can($permission)){
                return $next($request);
            }
        }
        abort(404);
    }
}
