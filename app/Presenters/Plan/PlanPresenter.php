<?php

namespace App\Presenters\Plan;

use App\Transformers\Plan\PlanTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PlanPresenter.
 *
 * @package namespace App\Presenters\Plan;
 */
class PlanPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PlanTransformer();
    }
}
