<?php

namespace App\Presenters\AccountsProperty;

use App\Transformers\AccountsProperty\AccountsPropertyTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountsPropertyPresenter.
 *
 * @package namespace App\Presenters\AccountsProperty;
 */
class AccountsPropertyPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountsPropertyTransformer();
    }
}
