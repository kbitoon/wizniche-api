<?php

namespace App\Presenters\EmployeesBusinessHour;

use App\Transformers\EmployeesBusinessHour\EmployeesBusinessHourTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmployeesBusinessHourPresenter.
 *
 * @package namespace App\Presenters\EmployeesBusinessHour;
 */
class EmployeesBusinessHourPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmployeesBusinessHourTransformer();
    }
}
