<?php

namespace App\Presenters\Position;

use App\Transformers\Position\PositionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PositionPresenter.
 *
 * @package namespace App\Presenters\Position;
 */
class PositionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PositionTransformer();
    }
}
