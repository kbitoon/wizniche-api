<?php

namespace App\Presenters\ServiceArea;

use App\Transformers\ServiceArea\ServiceAreaTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ServiceAreaPresenter.
 *
 * @package namespace App\Presenters\ServiceArea;
 */
class ServiceAreaPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ServiceAreaTransformer();
    }
}
