<?php

namespace App\Presenters\Service;

use App\Transformers\Service\ServiceTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ServicePresenter.
 *
 * @package namespace App\Presenters\Service;
 */
class ServicePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ServiceTransformer();
    }
}
