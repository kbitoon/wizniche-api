<?php

namespace App\Presenters\PaymentTransaction;

use App\Transformers\PaymentTransaction\PaymentTransactionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PaymentTransactionPresenter.
 *
 * @package namespace App\Presenters\PaymentTransaction;
 */
class PaymentTransactionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentTransactionTransformer();
    }
}
