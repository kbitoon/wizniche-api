<?php

namespace App\Presenters\AccountsPlans;

use App\Transformers\AccountsPlans\AccountsPlansTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountsPlansPresenter.
 *
 * @package namespace App\Presenters\AccountsPlans;
 */
class AccountsPlansPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountsPlansTransformer();
    }
}
