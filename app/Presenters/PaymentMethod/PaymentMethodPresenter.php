<?php

namespace App\Presenters\PaymentMethod;

use App\Transformers\PaymentMethod\PaymentMethodTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PaymentMethodPresenter.
 *
 * @package namespace App\Presenters\PaymentMethod;
 */
class PaymentMethodPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentMethodTransformer();
    }
}
