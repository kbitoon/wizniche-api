<?php

namespace App\Presenters\PasswordReset;

use App\Transformers\PasswordReset\PasswordResetTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PasswordResetPresenter.
 *
 * @package namespace App\Presenters\PasswordReset;
 */
class PasswordResetPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PasswordResetTransformer();
    }
}
