<?php

namespace App\Presenters\BusinessHour;

use App\Transformers\BusinessHour\BusinessHourTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BusinessHourPresenter.
 *
 * @package namespace App\Presenters\BusinessHour;
 */
class BusinessHourPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BusinessHourTransformer();
    }
}
