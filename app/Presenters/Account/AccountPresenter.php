<?php

namespace App\Presenters\Account;

use App\Transformers\Account\AccountTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountPresenter.
 *
 * @package namespace App\Presenters\Account;
 */
class AccountPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountTransformer();
    }
}
