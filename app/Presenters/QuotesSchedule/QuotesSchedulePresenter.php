<?php

namespace App\Presenters\QuotesSchedule;

use App\Transformers\QuotesSchedule\QuotesScheduleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class QuotesSchedulePresenter.
 *
 * @package namespace App\Presenters\QuotesSchedule;
 */
class QuotesSchedulePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new QuotesScheduleTransformer();
    }
}
