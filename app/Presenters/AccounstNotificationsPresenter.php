<?php

namespace App\Presenters;

use App\Transformers\AccounstNotificationsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccounstNotificationsPresenter.
 *
 * @package namespace App\Presenters;
 */
class AccounstNotificationsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccounstNotificationsTransformer();
    }
}
