<?php

namespace App\Presenters\ZipCode;

use App\Transformers\ZipCode\ZipCodeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ZipCodePresenter.
 *
 * @package namespace App\Presenters\ZipCode;
 */
class ZipCodePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ZipCodeTransformer();
    }
}
