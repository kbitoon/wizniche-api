<?php

namespace App\Presenters\ServiceAreaDetail;

use App\Transformers\ServiceAreaDetail\ServiceAreasDetailTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ServiceAreasDetailPresenter.
 *
 * @package namespace App\Presenters\ServiceAreaDetail;
 */
class ServiceAreasDetailPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ServiceAreasDetailTransformer();
    }
}
