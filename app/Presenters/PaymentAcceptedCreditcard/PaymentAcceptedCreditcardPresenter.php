<?php

namespace App\Presenters\PaymentAcceptedCreditcard;

use App\Transformers\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PaymentAcceptedCreditcardPresenter.
 *
 * @package namespace App\Presenters\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcardPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PaymentAcceptedCreditcardTransformer();
    }
}
