<?php

namespace App\Presenters\CustomerType;

use App\Transformers\CustomerType\CustomerTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomerTypePresenter.
 *
 * @package namespace App\Presenters\CustomerType;
 */
class CustomerTypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomerTypeTransformer();
    }
}
