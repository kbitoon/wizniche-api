<?php

namespace App\Presenters\EmployeesCertification;

use App\Transformers\EmployeesCertification\EmployeesCertificationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class EmployeesCertificationPresenter.
 *
 * @package namespace App\Presenters\EmployeesCertification;
 */
class EmployeesCertificationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new EmployeesCertificationTransformer();
    }
}
