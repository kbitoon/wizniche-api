<?php

namespace App\Presenters\AccountsNotifications;

use App\Transformers\AccountsNotifications\AccountsNotificationsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountsNotificationsPresenter.
 *
 * @package namespace App\Presenters\AccountsNotifications;
 */
class AccountsNotificationsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountsNotificationsTransformer();
    }
}
