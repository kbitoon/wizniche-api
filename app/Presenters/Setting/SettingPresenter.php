<?php

namespace App\Presenters\Setting;

use App\Transformers\Setting\SettingTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SettingPresenter.
 *
 * @package namespace App\Presenters\Setting;
 */
class SettingPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SettingTransformer();
    }
}
