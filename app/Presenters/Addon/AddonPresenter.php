<?php

namespace App\Presenters\Addon;

use App\Transformers\Addon\AddonTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AddonPresenter.
 *
 * @package namespace App\Presenters\Addon;
 */
class AddonPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AddonTransformer();
    }
}
