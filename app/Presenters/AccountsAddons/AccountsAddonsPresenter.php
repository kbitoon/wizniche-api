<?php

namespace App\Presenters\AccountsAddons;

use App\Transformers\AccountsAddons\AccountsAddonsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountsAddonsPresenter.
 *
 * @package namespace App\Presenters\AccountsAddons;
 */
class AccountsAddonsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountsAddonsTransformer();
    }
}
