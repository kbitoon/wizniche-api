<?php

namespace App\Presenters\Qoute;

use App\Transformers\Qoute\QuoteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class QuotePresenter.
 *
 * @package namespace App\Presenters\Qoute;
 */
class QuotePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new QuoteTransformer();
    }
}
