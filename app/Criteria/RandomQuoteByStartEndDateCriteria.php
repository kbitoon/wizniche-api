<?php

namespace App\Criteria;

use DateTime;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RandomQuoteByStartEndDateCriteria.
 *
 * @package namespace App\Criteria;
 */
class RandomQuoteByStartEndDateCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     * @throws \Exception
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $currentDate = new DateTime();

        $model = $model->whereHas('quotesSchedules', function($query) use ($currentDate) {
            $query->whereDate('start_date', '<=', $currentDate->format('Y-m-d'));
            $query->whereDate('end_date', '>=', $currentDate->format('Y-m-d'));
        });
        return $model;
    }
}
