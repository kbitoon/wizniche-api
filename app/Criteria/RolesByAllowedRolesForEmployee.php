<?php

namespace App\Criteria;

use App\Models\Role\Role;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RolesByAllowedPubliclyCriteria.
 *
 * @package namespace App\Criteria;
 */
class RolesByAllowedRolesForEmployee implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('slug', Role::ROLE_TECHNICIAN)
            ->orWhere('slug', Role::ROLE_ADMINISTRATOR);
        return $model;
    }
}
