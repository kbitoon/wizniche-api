<?php

namespace App\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EmployeeByAccountCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserByAccountCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = Auth::user();

        if ($user && $user->account_id) {
            $model = $model->where('account_id', $user->account_id);
        }

        return $model;
    }
}
