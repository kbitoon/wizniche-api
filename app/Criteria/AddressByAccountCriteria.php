<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class AddressByAccountCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * AddressByAccountCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user =  Auth::user();

        if ($this->request->has('addressable_type')) {
            // For Customers
            if ($this->request->get('addressable_type') == 'customers') {
                $model = $model->orWhereHas('customers', function($query) use ($user) {
                    $query->where('account_id', $user->account_id);
                });
            }

            // For Employees
            if ($this->request->get('addressable_type') == 'users') {
                $model = $model->orWhereHas('users', function($query) use ($user) {
                    $query->where('account_id', $user->account_id);
                });
            }
        } else {
            $model = $model->whereHas('accounts', function ($query) use ($user) {
                $query->where('id', $user->account_id);
            });
        }

        return $model;
    }
}
