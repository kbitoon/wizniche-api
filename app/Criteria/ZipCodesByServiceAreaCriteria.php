<?php

namespace App\Criteria;

use App\Models\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ZipCodesByServiceAreaCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
class ZipCodesByServiceAreaCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * ZipCodesByServiceAreaCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->request->has('service_area_id')) {
            $serviceArea = Service::find($this->request->get('service_area_id'))->where('account_id', Auth::user()->id);
            if ($serviceArea) {
                $model = $model->where('service_area_id', $this->request->get('service_area_id'));
            }
        }
        return $model;
    }
}
