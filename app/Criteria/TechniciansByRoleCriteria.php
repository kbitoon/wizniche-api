<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class TechniciansByRoleCriteria.
 *
 * @package namespace App\Criteria;
 */
class TechniciansByRoleCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereHas('roles', function($query) {
            $query->where('slug', 'technician');
            $query->orWhere('name', 'Technician');
        });
        return $model;
    }
}
