<?php

namespace App\Criteria;

use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ServiceByAccountCriteria.
 *
 * @package namespace App\Criteria;
 */
class ServiceByAccountCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user =  Auth::user();
        $model = $model->where(function ($query) use ($user) {
            $query->whereNull('account_id')
                ->orWhere('account_id', $user->account_id);
        });

        return $model;
    }
}
