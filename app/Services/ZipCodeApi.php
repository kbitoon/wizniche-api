<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;

class ZipCodeApi
{
    /**
     * @var string
     */
    private $zipCodeBaseUrl = '';

    /**
     * @var array
     */
    private $formatValid = array('json','xml','csv');

    /**
     * @var array
     */
    private $unitsValid = array('km','mile','degrees','radians');

    /**
     * @var HttpClient
     */
    private $client;

    /**
     * ZipCodeApi constructor.
     */
    public function __construct()
    {
        $this->zipCodeBaseUrl = env('ZIP_CODE_API_URL').'/'. env('ZIP_CODE_API_KEY').'/';
        $this->client = new HttpClient();
    }

    /**
     * @param $zipcode
     * @return bool
     * @throws Exception
     */
    private function checkZipCode($zipcode)
    {
        if(!preg_match('/^\d{5}$/', $zipcode)) {
            throw new Exception('Zip code is not valid.');
        }
        return true;
    }

    /**
     * @param $zipcodes
     * @return bool
     * @throws Exception
     */
    private function checkZipCodes($zipcodes)
    {
        $zips = explode(',', $zipcodes);
        foreach($zips as $zip) {
            $this->checkZipCode($zip);
        }
        return true;
    }

    /**
     * @param $state
     * @return bool
     * @throws Exception
     */
    private function checkState($state)
    {
        if(strlen($state) > 2) {
            throw new Exception('Must use abbreviated form of state');
        }
        return true;
    }

    /**
     * @param $units
     * @return bool
     * @throws Exception
     */
    private function checkUnits($units)
    {
        if(!in_array($units,$this->unitsValid)) {
            throw new Exception('Unit is not valid. Must be set as one of the following: '.implode(', ',$this->unitsValid).'');
        }
        return true;
    }

    /**
     * @param $format
     * @return bool
     * @throws Exception
     */
    private function checkFormat($format)
    {
        if(!in_array($format,$this->formatValid)) {
            throw new Exception('Format is not valid. Must be set as one of the following: ' . implode(', ', $this->formatValid) . '');
        }
        return true;
    }

    /**
     * @param $distance
     * @return bool
     * @throws Exception
     */
    private function checkDistance($distance)
    {
        if(!is_numeric($distance)) {
            throw new Exception('Distance is not valid.');
        }

        return true;
    }

    /**
     * @param $string
     * @return string
     */
    private function prepareString($string)
    {
        $string = strtolower($string);
        return $string;
    }

    /**
     * @param $url
     * @return string
     * @throws GuzzleException
     */
    protected function request($url)
    {
        $res = $this->client->request('GET', $url);
        return $res->getBody()->getContents();
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/distance.<format>/<zip_code1>/<zip_code2>/<units>
     * Returns 'distance' between the two provided zip codes in the format requested. Default set to json.
     * @param $zipcode
     * @param $zipcode2
     * @param string $units
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getZipCodeDistance($zipcode,$zipcode2,$units = 'mile',$format = 'json')
    {
        $format = $this->prepareString($format);
        $units = $this->prepareString($units);
        $this->checkZipCode($zipcode);
        $this->checkZipCode($zipcode2);
        $this->checkUnits($units);
        $this->checkFormat($format);

        $sendUrl = $this->zipCodeBaseUrl.'distance.'.$format.'/'.$zipcode.'/'.$zipcode2.'/'.$units;
        return $this->request($sendUrl);
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/radius.<format>/<zip_code>/<distance>/<units>
     * Returns zipcodes within a given radius of proivided zipcode.
     * @param $zipcode
     * @param int $distance
     * @param string $units
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getZipCodesByRadius($zipcode,$distance = 10,$units = 'mile',$format = 'json')
    {
        $format = $this->prepareString($format);
        $units = $this->prepareString($units);
        $this->checkZipCode($zipcode);
        $this->checkDistance($distance);
        $this->checkUnits($units);
        $this->checkFormat($format);

        $sendUrl = $this->zipCodeBaseUrl.'radius.'.$format.'/'.$zipcode.'/'.$distance.'/'.$units;
        return $this->request($sendUrl);
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/info.<format>/<zip_code>/<units>
     * Returns city, state, latitude, longitude, and time zone information for a zip code.
     * The JSON and XML responses will contain alternative acceptable city names for a location. CSV will not.
     * @param $zipcode
     * @param string $units
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getLocationInfoByZipCode($zipcode,$units = 'degrees',$format = 'json')
    {
        $format = $this->prepareString($format);
        $units = $this->prepareString($units);
        $this->checkZipCode($zipcode);
        $this->checkUnits($units);
        $this->checkFormat($format);

        $sendUrl = $this->zipCodeBaseUrl.'info.'.$format.'/'.$zipcode.'/'.$units;
        return $this->request($sendUrl);
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/city-zips.<format>/<city>/<state>
     * Returns possible zip codes for a city.
     * @param $city
     * @param $state
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getZipCodesByLocation($city,$state,$format = 'json')
    {
        $format = $this->prepareString($format);
        $city = $this->prepareString($city);
        $state = $this->prepareString($state);
        $this->checkFormat($format);
        $this->checkState($state);

        $sendUrl = $this->zipCodeBaseUrl.'city-zips.'.$format.'/'.$city.'/'.$state;
        return $this->request($sendUrl);
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/state-zips.<format>/<state>
     * Returns possible zip codes for a state.
     * @param $state
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getZipCodesByState($state,$format = 'json')
    {
        $format = $this->prepareString($format);
        $state = $this->prepareString($state);
        $this->checkFormat($format);
        $this->checkState($state);

        $sendUrl = $this->zipCodeBaseUrl.'state-zips.'.$format.'/'.$state;
        return $this->request($sendUrl);
    }

    /**
     * URL format http://www.zipcodeapi.com/rest/<api_key>/multi-distance.<format>/<zip_code>/<other_zip_codes/units>
     * Returns distance between one zip code and multiple other zip codes.
     * @param $zipcode
     * @param $zipcodes
     * @param string $units
     * @param string $format
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getMultipleZipCodesDistance($zipcode, $zipcodes, $units = 'mile', $format = 'json')
    {
        $format = $this->prepareString($format);
        $units = $this->prepareString($units);
        $this->checkZipCode($zipcode);
        $this->checkZipCodes($zipcodes);
        $this->checkFormat($format);
        $this->checkUnits($units);

        $sendUrl = $this->zipCodeBaseUrl.'multi-distance.'.$format.'/'.$zipcode.'/'.$zipcodes.'/'.$units;
        return $this->request($sendUrl);
    }
}
