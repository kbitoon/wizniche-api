<?php

namespace App\Services;

use Exception;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;

class MelissaDataApi
{
    /**
     * @var string
     */
    private $zipCodeBaseUrl = '';

    /**
     * @var array
     */
    private $formatValid = array('json','xml');


    /**
     * @var HttpClient
     */
    private $client;

    /**
     * ZipCodeApi constructor.
     */
    public function __construct()
    {
        $this->zipCodeBaseUrl = env('MELISSA_DATA_API_URL').'/';
        $this->client = new HttpClient();
    }

    /**
     * @param $url
     * @return string
     * @throws GuzzleException
     */
    protected function request($url)
    {
        $res = $this->client->request('GET', $url);
        return $res->getBody()->getContents();
    }

    /**
     * @param $params
     * @return string
     * @throws GuzzleException
     */
    public function getPropertyInformationByAddress($params)
    {
        $defaultParams = [
            'address' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'mak' => '',
            'freeForm' => '',
            'fmt' => 'json'
        ];

        if (is_array($params)) {
            $params = array_merge($defaultParams, $params);
        } else {
            $params = $defaultParams;
        }

        $sendUrl = $this->zipCodeBaseUrl.'/property/'.'?address='.$params['address'].'&city='.$params['city'].'&state='.$params['state'].'&zip='.$params['zip'].'&make='.$params['mak'] .'&freeForm='.$params['freeForm'].'&fmt='.$params['fmt'].'&id='.env('MELISSA_DATA_API_LICENSE_KEY');
        return $this->request($sendUrl);
    }

    /**
     * @param $zipcode
     * @param int $radius
     * @param string $fmt
     * @return string
     * @throws GuzzleException
     * @throws Exception
     */
    public function getZipCodesByRadius($zipcode, $radius = 10, $fmt = 'json')
    {
        $sendUrl = $this->zipCodeBaseUrl.'/zipradius/zipcode/'.'?zipcode='.$zipcode.'radius='.$radius.'&fmt='.$fmt.'&id='.env('MELISSA_DATA_API_LICENSE_KEY');
        return $this->request($sendUrl);
    }
}
