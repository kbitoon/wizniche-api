<?php

namespace App\Repositories\Account;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountRepository.
 *
 * @package namespace App\Repositories\Account;
 */
interface AccountRepository extends RepositoryInterface
{
    //
}
