<?php

namespace App\Repositories\ZipCode;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ZipCodeRepository.
 *
 * @package namespace App\Repositories\ZipCode;
 */
interface ZipCodeRepository extends RepositoryInterface
{
    //
}
