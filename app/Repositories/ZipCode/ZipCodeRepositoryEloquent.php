<?php

namespace App\Repositories\ZipCode;

use App\Criteria\ZipCodesByServiceAreaCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ZipCode\ZipCode;
use App\Validators\ZipCode\ZipCodeValidator;

/**
 * Class ZipCodeRepositoryEloquent.
 *
 * @package namespace App\Repositories\ZipCode;
 */
class ZipCodeRepositoryEloquent extends BaseRepository implements ZipCodeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ZipCode::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ZipCodeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(ZipCodesByServiceAreaCriteria::class));
    }
    
}
