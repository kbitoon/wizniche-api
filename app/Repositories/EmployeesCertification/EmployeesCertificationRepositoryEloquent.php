<?php

namespace App\Repositories\EmployeesCertification;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\EmployeesCertification\EmployeesCertification;
use App\Validators\EmployeesCertification\EmployeesCertificationValidator;

/**
 * Class EmployeesCertificationRepositoryEloquent.
 *
 * @package namespace App\Repositories\EmployeesCertification;
 */
class EmployeesCertificationRepositoryEloquent extends BaseRepository implements EmployeesCertificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeesCertification::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmployeesCertificationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
