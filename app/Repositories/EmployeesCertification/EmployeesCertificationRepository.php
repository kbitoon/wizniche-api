<?php

namespace App\Repositories\EmployeesCertification;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmployeesCertificationRepository.
 *
 * @package namespace App\Repositories\EmployeesCertification;
 */
interface EmployeesCertificationRepository extends RepositoryInterface
{
    //
}
