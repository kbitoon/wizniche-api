<?php

namespace App\Repositories\PaymentMethod;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentMethodRepository.
 *
 * @package namespace App\Repositories\PaymentMethod;
 */
interface PaymentMethodRepository extends RepositoryInterface
{
    //
}
