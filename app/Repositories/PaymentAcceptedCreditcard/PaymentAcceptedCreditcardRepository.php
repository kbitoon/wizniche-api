<?php

namespace App\Repositories\PaymentAcceptedCreditcard;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentAcceptedCreditcardRepository.
 *
 * @package namespace App\Repositories\PaymentAcceptedCreditcard;
 */
interface PaymentAcceptedCreditcardRepository extends RepositoryInterface
{
    //
}
