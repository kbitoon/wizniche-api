<?php

namespace App\Repositories\PaymentAcceptedCreditcard;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\PaymentAcceptedCreditcard\PaymentAcceptedCreditcard;
use App\Validators\PaymentAcceptedCreditcard\PaymentAcceptedCreditcardValidator;

/**
 * Class PaymentAcceptedCreditcardRepositoryEloquent.
 *
 * @package namespace App\Repositories\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcardRepositoryEloquent extends BaseRepository implements PaymentAcceptedCreditcardRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentAcceptedCreditcard::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PaymentAcceptedCreditcardValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
