<?php

namespace App\Repositories\ServiceArea;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServiceAreaRepository.
 *
 * @package namespace App\Repositories\ServiceArea;
 */
interface ServiceAreaRepository extends RepositoryInterface
{
    //
}
