<?php

namespace App\Repositories\ServiceArea;

use App\Criteria\ServiceAreasByAccountCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ServiceArea\ServiceArea;
use App\Validators\ServiceArea\ServiceAreaValidator;

/**
 * Class ServiceAreaRepositoryEloquent.
 *
 * @package namespace App\Repositories\ServiceArea;
 */
class ServiceAreaRepositoryEloquent extends BaseRepository implements ServiceAreaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ServiceArea::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ServiceAreaValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(ServiceAreasByAccountCriteria::class));
    }
    
}
