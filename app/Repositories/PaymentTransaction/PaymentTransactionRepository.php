<?php

namespace App\Repositories\PaymentTransaction;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentTransactionRepository.
 *
 * @package namespace App\Repositories\PaymentTransaction;
 */
interface PaymentTransactionRepository extends RepositoryInterface
{
    //
}
