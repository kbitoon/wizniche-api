<?php

namespace App\Repositories\PaymentTransaction;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\PaymentTransaction\PaymentTransaction;
use App\Validators\PaymentTransaction\PaymentTransactionValidator;

/**
 * Class PaymentTransactionRepositoryEloquent.
 *
 * @package namespace App\Repositories\PaymentTransaction;
 */
class PaymentTransactionRepositoryEloquent extends BaseRepository implements PaymentTransactionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PaymentTransaction::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return PaymentTransactionValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
