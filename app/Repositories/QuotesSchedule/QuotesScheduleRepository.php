<?php

namespace App\Repositories\QuotesSchedule;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuotesScheduleRepository.
 *
 * @package namespace App\Repositories\QuotesSchedule;
 */
interface QuotesScheduleRepository extends RepositoryInterface
{
    //
}
