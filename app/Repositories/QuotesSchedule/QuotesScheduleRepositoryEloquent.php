<?php

namespace App\Repositories\QuotesSchedule;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\QuotesSchedule\QuotesSchedule;
use App\Validators\QuotesSchedule\QuotesScheduleValidator;

/**
 * Class QuotesScheduleRepositoryEloquent.
 *
 * @package namespace App\Repositories\QuotesSchedule;
 */
class QuotesScheduleRepositoryEloquent extends BaseRepository implements QuotesScheduleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuotesSchedule::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuotesScheduleValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
