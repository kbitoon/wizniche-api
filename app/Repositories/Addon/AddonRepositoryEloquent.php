<?php

namespace App\Repositories\Addon;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Addon\Addon;
use App\Validators\Addon\AddonValidator;

/**
 * Class AddonRepositoryEloquent.
 *
 * @package namespace App\Repositories\Addon;
 */
class AddonRepositoryEloquent extends BaseRepository implements AddonRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Addon::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AddonValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
