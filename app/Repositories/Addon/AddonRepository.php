<?php

namespace App\Repositories\Addon;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AddonRepository.
 *
 * @package namespace App\Repositories\Addon;
 */
interface AddonRepository extends RepositoryInterface
{
    //
}
