<?php

namespace App\Repositories\Service;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServiceRepository.
 *
 * @package namespace App\Repositories\Service;
 */
interface ServiceRepository extends RepositoryInterface
{
    //
}
