<?php

namespace App\Repositories\EmployeesBusinessHour;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\EmployeesBusinessHour\EmployeesBusinessHour;
use App\Validators\EmployeesBusinessHour\EmployeesBusinessHourValidator;

/**
 * Class EmployeesBusinessHourRepositoryEloquent.
 *
 * @package namespace App\Repositories\EmployeesBusinessHour;
 */
class EmployeesBusinessHourRepositoryEloquent extends BaseRepository implements EmployeesBusinessHourRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EmployeesBusinessHour::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EmployeesBusinessHourValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
