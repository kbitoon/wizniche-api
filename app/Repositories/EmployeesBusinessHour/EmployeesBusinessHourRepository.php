<?php

namespace App\Repositories\EmployeesBusinessHour;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EmployeesBusinessHourRepository.
 *
 * @package namespace App\Repositories\EmployeesBusinessHour;
 */
interface EmployeesBusinessHourRepository extends RepositoryInterface
{
    //
}
