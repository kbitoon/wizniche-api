<?php

namespace App\Repositories\ServiceAreaDetail;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use App\Validators\ServiceAreaDetail\ServiceAreasDetailValidator;

/**
 * Class ServiceAreasDetailRepositoryEloquent.
 *
 * @package namespace App\Repositories\ServiceAreaDetail;
 */
class ServiceAreasDetailRepositoryEloquent extends BaseRepository implements ServiceAreasDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ServiceAreasDetail::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ServiceAreasDetailValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
