<?php

namespace App\Repositories\ServiceAreaDetail;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ServiceAreasDetailRepository.
 *
 * @package namespace App\Repositories\ServiceAreaDetail;
 */
interface ServiceAreasDetailRepository extends RepositoryInterface
{
    //
}
