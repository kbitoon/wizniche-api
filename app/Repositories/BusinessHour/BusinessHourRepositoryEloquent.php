<?php

namespace App\Repositories\BusinessHour;

use App\Criteria\BusinessHourByAccountCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\BusinessHour\BusinessHour;
use App\Validators\BusinessHour\BusinessHourValidator;

/**
 * Class BusinessHourRepositoryEloquent.
 *
 * @package namespace App\Repositories\BusinessHour;
 */
class BusinessHourRepositoryEloquent extends BaseRepository implements BusinessHourRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BusinessHour::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return BusinessHourValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(BusinessHourByAccountCriteria::class);
    }
    
}
