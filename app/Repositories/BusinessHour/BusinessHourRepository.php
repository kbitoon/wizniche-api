<?php

namespace App\Repositories\BusinessHour;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BusinessHourRepository.
 *
 * @package namespace App\Repositories\BusinessHour;
 */
interface BusinessHourRepository extends RepositoryInterface
{
    //
}
