<?php

namespace App\Repositories\Qoute;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuoteRepository.
 *
 * @package namespace App\Repositories\Qoute;
 */
interface QuoteRepository extends RepositoryInterface
{
    //
}
