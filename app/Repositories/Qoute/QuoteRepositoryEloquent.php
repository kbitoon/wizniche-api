<?php

namespace App\Repositories\Qoute;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Qoute\Quote;
use App\Validators\Qoute\QuoteValidator;

/**
 * Class QuoteRepositoryEloquent.
 *
 * @package namespace App\Repositories\Qoute;
 */
class QuoteRepositoryEloquent extends BaseRepository implements QuoteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Quote::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return QuoteValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
