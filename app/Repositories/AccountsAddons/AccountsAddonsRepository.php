<?php

namespace App\Repositories\AccountsAddons;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountsAddonsRepository.
 *
 * @package namespace App\Repositories\AccountsAddons;
 */
interface AccountsAddonsRepository extends RepositoryInterface
{
    //
}
