<?php

namespace App\Repositories\AccountsAddons;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AccountsAddons\AccountsAddons;
use App\Validators\AccountsAddons\AccountsAddonsValidator;

/**
 * Class AccountsAddonsRepositoryEloquent.
 *
 * @package namespace App\Repositories\AccountsAddons;
 */
class AccountsAddonsRepositoryEloquent extends BaseRepository implements AccountsAddonsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AccountsAddons::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AccountsAddonsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
