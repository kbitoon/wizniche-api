<?php

namespace App\Repositories\CustomerType;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\CustomerType\CustomerType;
use App\Validators\CustomerType\CustomerTypeValidator;

/**
 * Class CustomerTypeRepositoryEloquent.
 *
 * @package namespace App\Repositories\CustomerType;
 */
class CustomerTypeRepositoryEloquent extends BaseRepository implements CustomerTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CustomerType::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CustomerTypeValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
