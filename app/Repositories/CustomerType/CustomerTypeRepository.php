<?php

namespace App\Repositories\CustomerType;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomerTypeRepository.
 *
 * @package namespace App\Repositories\CustomerType;
 */
interface CustomerTypeRepository extends RepositoryInterface
{
    //
}
