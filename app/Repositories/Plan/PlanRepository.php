<?php

namespace App\Repositories\Plan;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanRepository.
 *
 * @package namespace App\Repositories\Plan;
 */
interface PlanRepository extends RepositoryInterface
{
    //
}
