<?php

namespace App\Repositories\AccountsProperty;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountsPropertyRepository.
 *
 * @package namespace App\Repositories\AccountsProperty;
 */
interface AccountsPropertyRepository extends RepositoryInterface
{
    //
}
