<?php

namespace App\Repositories\AccountsProperty;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AccountsProperty\AccountsProperty;
use App\Validators\AccountsProperty\AccountsPropertyValidator;

/**
 * Class AccountsPropertyRepositoryEloquent.
 *
 * @package namespace App\Repositories\AccountsProperty;
 */
class AccountsPropertyRepositoryEloquent extends BaseRepository implements AccountsPropertyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AccountsProperty::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AccountsPropertyValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
