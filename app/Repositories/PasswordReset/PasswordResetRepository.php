<?php

namespace App\Repositories\PasswordReset;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PasswordResetRepository.
 *
 * @package namespace App\Repositories\PasswordReset;
 */
interface PasswordResetRepository extends RepositoryInterface
{
    //
}
