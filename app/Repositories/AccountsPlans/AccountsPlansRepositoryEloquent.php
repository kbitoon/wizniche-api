<?php

namespace App\Repositories\AccountsPlans;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AccountsPlans\AccountsPlans;
use App\Validators\AccountsPlans\AccountsPlansValidator;

/**
 * Class AccountsPlansRepositoryEloquent.
 *
 * @package namespace App\Repositories\AccountsPlans;
 */
class AccountsPlansRepositoryEloquent extends BaseRepository implements AccountsPlansRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AccountsPlans::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AccountsPlansValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
