<?php

namespace App\Repositories\AccountsPlans;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountsPlansRepository.
 *
 * @package namespace App\Repositories\AccountsPlans;
 */
interface AccountsPlansRepository extends RepositoryInterface
{
    //
}
