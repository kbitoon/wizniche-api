<?php

namespace App\Repositories\AccountsNotifications;

use App\Criteria\NotificationByAccountCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AccountsNotifications\AccountsNotifications;
use App\Validators\AccountsNotifications\AccountsNotificationsValidator;

/**
 * Class AccountsNotificationsRepositoryEloquent.
 *
 * @package namespace App\Repositories\AccountsNotifications;
 */
class AccountsNotificationsRepositoryEloquent extends BaseRepository implements AccountsNotificationsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AccountsNotifications::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AccountsNotificationsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(NotificationByAccountCriteria::class);
    }
    
}
