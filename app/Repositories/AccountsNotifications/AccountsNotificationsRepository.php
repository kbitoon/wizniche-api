<?php

namespace App\Repositories\AccountsNotifications;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountsNotificationsRepository.
 *
 * @package namespace App\Repositories\AccountsNotifications;
 */
interface AccountsNotificationsRepository extends RepositoryInterface
{
    //
}
