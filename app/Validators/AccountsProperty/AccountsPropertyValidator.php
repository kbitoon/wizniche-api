<?php

namespace App\Validators\AccountsProperty;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AccountsPropertyValidator.
 *
 * @package namespace App\Validators\AccountsProperty;
 */
class AccountsPropertyValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id'  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id'  => 'required',
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account'
    ];
}
