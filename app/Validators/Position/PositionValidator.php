<?php

namespace App\Validators\Position;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PositionValidator.
 *
 * @package namespace App\Validators\Position;
 */
class PositionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id' => 'required',
            'name' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id' => 'required',
            'name' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'name' => 'Position'
    ];
}
