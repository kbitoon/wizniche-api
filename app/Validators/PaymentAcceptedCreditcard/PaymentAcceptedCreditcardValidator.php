<?php

namespace App\Validators\PaymentAcceptedCreditcard;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PaymentAcceptedCreditcardValidator.
 *
 * @package namespace App\Validators\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcardValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
