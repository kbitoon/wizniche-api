<?php

namespace App\Validators\EmployeesCertification;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmployeesCertificationValidator.
 *
 * @package namespace App\Validators\EmployeesCertification;
 */
class EmployeesCertificationValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'required',
            'name' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'user_id' => 'required',
            'name' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'user_id' => 'Employee',
        'name' => 'Name'
    ];
}
