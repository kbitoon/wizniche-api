<?php

namespace App\Validators\Setting;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class SettingValidator.
 *
 * @package namespace App\Validators\Setting;
 */
class SettingValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|unique:settings',
            'data_type' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required|unique:settings',
            'data_type' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'name' => 'Name',
        'data_type' => 'Data Type'
    ];
}
