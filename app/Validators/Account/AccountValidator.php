<?php

namespace App\Validators\Account;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AccountValidator.
 *
 * @package namespace App\Validators\Account;
 */
class AccountValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'business_name' => 'required',
            'business_phone' => 'required',
            'support_email' => 'email'
        ],
        ValidatorInterface::RULE_UPDATE => [],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'business_name' => 'Business Name',
        'business_phone' => 'Business Phone',
        'support_email' => 'Support Email'
    ];
}
