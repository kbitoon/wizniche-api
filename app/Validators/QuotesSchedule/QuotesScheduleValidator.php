<?php

namespace App\Validators\QuotesSchedule;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class QuotesScheduleValidator.
 *
 * @package namespace App\Validators\QuotesSchedule;
 */
class QuotesScheduleValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'start_date' => 'required',
            'end_date' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'start_date' => 'required',
            'end_date' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'start_date' => 'Start Date',
        'end_date' => 'End Date'
    ];
}
