<?php

namespace App\Validators\AccountsPlans;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AccountsPlansValidator.
 *
 * @package namespace App\Validators\AccountsPlans;
 */
class AccountsPlansValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id' => 'required',
            'plan_id' => 'required',
            'price' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id' => 'required',
            'plan_id' => 'required',
            'price' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'plan_id' => 'Plan',
        'price' => 'Price'
    ];
}
