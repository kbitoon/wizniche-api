<?php

namespace App\Validators\EmployeesBusinessHour;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EmployeesBusinessHourValidator.
 *
 * @package namespace App\Validators\EmployeesBusinessHour;
 */
class EmployeesBusinessHourValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'user_id' => 'required',
            'day' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'user_id' => 'required',
            'day' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'user_id' => 'User',
        'day' => 'Day'
    ];
}
