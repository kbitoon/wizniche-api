<?php

namespace App\Validators\ServiceArea;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ServiceAreaValidator.
 *
 * @package namespace App\Validators\ServiceArea;
 */
class ServiceAreaValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id' => 'required',
            'address_id' => 'required',
            'type' => 'required',
            'mile_radius' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id' => 'required',
            'address_id' => 'required',
            'type' => 'required',
            'mile_radius' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'address_id' => 'Address',
        'type' => 'Type',
        'mile_radius' => 'Mile Radius'
    ];
}
