<?php

namespace App\Validators\BusinessHour;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class BusinessHourValidator.
 *
 * @package namespace App\Validators\BusinessHour;
 */
class BusinessHourValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id' => 'required',
            'type' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id' => 'required',
            'type' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'type' => 'Type'
    ];
}
