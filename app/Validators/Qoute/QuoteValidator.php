<?php

namespace App\Validators\Qoute;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class QuoteValidator.
 *
 * @package namespace App\Validators\Qoute;
 */
class QuoteValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'quote' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'quote' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'quote' => 'Quote'
    ];
}
