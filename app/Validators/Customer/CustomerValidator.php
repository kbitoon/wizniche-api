<?php

namespace App\Validators\Customer;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CustomerValidator.
 *
 * @package namespace App\Validators\Customer;
 */
class CustomerValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email'
    ];
}
