<?php

namespace App\Validators\AccountsAddons;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AccountsAddonsValidator.
 *
 * @package namespace App\Validators\AccountsAddons;
 */
class AccountsAddonsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id'  => 'required',
            'addon_id' => 'required',
            'price' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id'  => 'required',
            'addon_id' => 'required',
            'price' => 'required',
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'addon_id' => 'Addon',
        'price' => 'Price'
    ];
}
