<?php

namespace App\Validators\AccountsNotifications;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class AccountsNotificationsValidator.
 *
 * @package namespace App\Validators\AccountsNotifications;
 */
class AccountsNotificationsValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'account_id' => 'required',
            'group' => 'required',
            'slug' => 'required',
            'name' => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'account_id' => 'required',
            'group' => 'required',
            'slug' => 'required',
            'name' => 'required',
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'account_id' => 'Account',
        'group' => 'Group',
        'slug' => 'Slug',
        'name' => 'Name'
    ];
}
