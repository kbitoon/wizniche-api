<?php

namespace App\Validators\ZipCode;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ZipCodeValidator.
 *
 * @package namespace App\Validators\ZipCode;
 */
class ZipCodeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'service_area_id' => 'required',
            'code' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'service_area_id' => 'required',
            'code' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'service_area_id' => 'Service Area',
        'code' => 'Zip Code'
    ];
}
