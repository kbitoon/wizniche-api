<?php

namespace App\Validators\ServiceAreaDetail;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ServiceAreasDetailValidator.
 *
 * @package namespace App\Validators\ServiceAreaDetail;
 */
class ServiceAreasDetailValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'service_area_id' => 'required',
            'zone_name' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'service_area_id' => 'required',
            'zone_name' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'service_area_id' => 'Service Area',
        'zone_name' => 'Zone Name'
    ];
}
