<?php

namespace App\Validators\Plan;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PlanValidator.
 *
 * @package namespace App\Validators\Plan;
 */
class PlanValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required',
            'price' => 'required',
            'max_users' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required',
            'price' => 'required',
            'max_users' => 'required'
        ],
    ];

    /**
     * Get custom attributes for validator errors.
     *
     * @var array
     */
    protected $attributes = [
        'name' => 'Name',
        'price' => 'Price',
        'max_users' => 'Maximum Users'
    ];
}
