<?php

namespace App\Traits\Tags;

use App\Models\Tag\Tag;
use Illuminate\Support\Facades\Auth;

trait Tags
{
    /**
     * This will handle the creation/updating of tags
     * @param $entity
     */
    public function handleTags($entity)
    {
        if ($this->request->has('tags')) {
            $tagNames = $this->request->get('tags');
            $tags = [];
            foreach ($tagNames as $tagName) {
                $tag = Tag::firstOrCreate(
                    ['account_id' => Auth::user()->account_id, 'name' => $tagName]
                );
                $tags[] = $tag->id;
            }

            if ($tags && !empty($tags)) {
                $entity->tags()->sync($tags);
            } else {
                $entity->tags()->sync([]);
            }
        }
    }
}
