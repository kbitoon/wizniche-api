<?php

namespace App\Listeners;

use App\Models\User\User;
use App\Notifications\SendEmailVerificationNotification;
use App\Repositories\Account\AccountRepository;
use App\Repositories\AccountsPlans\AccountsPlansRepository;
use App\Repositories\Plan\PlanRepository;
use App\Repositories\Role\RoleRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RunPostRegistrationListener
{
    /**
     * @var RoleRepository
     */
    protected $roleResetRepository;

    /**
     * @var AccountsPlansRepository
     */
    protected $accountsPlansRepository;

    /**
     * @var AccountRepository
     */
    protected $accountRepository;

    /**
     * @var PlanRepository
     */
    protected $planRepository;

    /**
     * Create the event listener.
     *
     * @param AccountRepository $accountRepository
     * @param AccountsPlansRepository $accountsPlansRepository
     * @param PlanRepository $planRepository
     * @param RoleRepository $roleResetRepository
     */
    public function __construct(
        AccountRepository $accountRepository,
        AccountsPlansRepository $accountsPlansRepository,
        PlanRepository $planRepository,
        RoleRepository $roleResetRepository)
    {
        $this->accountRepository = $accountRepository;
        $this->accountsPlansRepository = $accountsPlansRepository;
        $this->planRepository = $planRepository;
        $this->roleResetRepository = $roleResetRepository;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Lets create our account
        $accountData = [
            'business_name' => 'Business Name',
            'business_phone' => 'Business Name'
        ];
        $account = $this->accountRepository->create($accountData);

        // Lets assign a 14 day trial plan for this account
        $plan = $this->planRepository->first();
        $accountPlansData = [
            'account_id' => $account->id,
            'plan_id' => $plan->id,
            'price' => 0.0,
            'max_users' => $plan->max_users,
            'unsubscribed_at' => Carbon::now()->addDay(14)
        ];

        $this->accountsPlansRepository->create($accountPlansData);

        // Lets create our user
        $userData = $event->request->all();
        $userData['account_id'] = $account->id;
        $userData['is_account_owner'] = true;
        $user = User::create($userData);

        // Assuming all registered users are subscribers
        $subscriber =  $this->roleResetRepository->findByField('slug', 'subscriber')->first();
        $user->roles()->attach($subscriber);

        // Send verification link email
        $user->notify(new SendEmailVerificationNotification());
    }
}
