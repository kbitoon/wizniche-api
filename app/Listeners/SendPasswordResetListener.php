<?php

namespace App\Listeners;

use App\Events\UserPasswordResetRequestEvent;
use App\Notifications\PasswordResetRequestNotification;
use App\Repositories\PasswordReset\PasswordResetRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class SendPasswordResetListener
{
    /**
     * @var PasswordResetRepository
     */
    protected $passwordResetRepository;

    /**
     * Create the event listener.
     *
     * @param PasswordResetRepository $passwordResetRepository
     */
    public function __construct(PasswordResetRepository $passwordResetRepository)
    {
        $this->passwordResetRepository = $passwordResetRepository;
    }

    /**
     * Handle the event.
     *
     * @param UserPasswordResetRequestEvent $event
     * @return void
     */
    public function handle(UserPasswordResetRequestEvent $event)
    {
        $item = $this->passwordResetRepository->updateOrCreate(
            ['email' => $event->user->email],
            [
                'email' => $event->user->email,
                'token' => Str::random(40)
            ]
        );

        $event->user->notify(new PasswordResetRequestNotification($item->token));
    }
}
