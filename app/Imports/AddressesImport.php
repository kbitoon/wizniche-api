<?php

namespace App\Imports;

use App\Models\Address\Address;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AddressesImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Address([
            'addressable_id' => $row['addressable_id'],
            'addressable_type' => $row['addressable_type'],
            'street' => $row['street'],
            'unit' => $row['unit'],
            'city' => $row['city'],
            'state' => $row['state'],
            'zip' => $row['zip'],
            'is_primary' => $row['is_primary'],
            'is_billing' => $row['is_billing'],
            'latitude' => $row['latitude'],
            'longitude' => $row['longitude']
        ]);
    }
}
