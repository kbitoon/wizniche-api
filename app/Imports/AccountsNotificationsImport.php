<?php

namespace App\Imports;

use App\Models\AccountsNotifications\AccountsNotifications;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AccountsNotificationsImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new AccountsNotifications([
            'account_id' => $row['account_id'],
            'group' => $row['group'],
            'slug' => $row['slug'],
            'name' => $row['name'],
            'notification_type' => $row['notification_type'],
            'context' => $row['context'],
            'is_enabled' => $row['is_enabled'],
        ]);
    }
}
