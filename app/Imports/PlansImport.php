<?php

namespace App\Imports;

use App\Models\Plan\Plan;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PlansImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Plan([
            'name' => $row['name'],
            'description' => $row['description'],
            'price' => $row['price'],
            'is_active' => $row['is_active'],
            'max_users' => $row['max_users']
        ]);
    }
}
