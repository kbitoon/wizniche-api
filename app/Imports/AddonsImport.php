<?php

namespace App\Imports;

use App\Models\Addon\Addon;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AddonsImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Addon([
            'name' => $row['name'],
            'description' => $row['description'],
            'price' => $row['price']
        ]);
    }
}
