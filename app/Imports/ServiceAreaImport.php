<?php

namespace App\Imports;

use App\Models\ServiceArea\ServiceArea;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ServiceAreaImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ServiceArea([
            'account_id' => $row['account_id'],
            'address_id' => $row['address_id'],
            'type' => $row['type'],
            'mile_radius' => $row['mile_radius']
        ]);
    }
}
