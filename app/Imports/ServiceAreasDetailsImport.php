<?php

namespace App\Imports;

use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ServiceAreasDetailsImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ServiceAreasDetail([
            'service_area_id' => $row['service_area_id'],
            'zone_name' => $row['zone_name'],
            'is_available_monday' => $row['is_available_monday'],
            'is_available_tuesday' => $row['is_available_tuesday'],
            'is_available_wednesday' => $row['is_available_wednesday'],
            'is_available_thursday' => $row['is_available_thursday'],
            'is_available_friday' => $row['is_available_friday'],
            'is_available_saturday' => $row['is_available_saturday'],
            'is_available_sunday' => $row['is_available_sunday']
        ]);
    }
}
