<?php

namespace App\Imports;

use App\Models\Customer\Customer;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CustomersImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Customer([
            'account_id' => $row['account_id'],
            'customer_type_id' => $row['customer_type_id'],
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'mobile_phone' => $row['mobile_phone'],
            'home_phone' => $row['home_phone'],
            'email' => $row['email']
        ]);
    }
}
