<?php

namespace App\Imports;

use App\Models\BusinessHour\BusinessHour;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BusinessHoursImport implements ToModel, WithHeadingRow
{
    use Importable;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new BusinessHour([
            'account_id' => $row['account_id'],
            'type' => $row['type']
        ]);
    }
}
