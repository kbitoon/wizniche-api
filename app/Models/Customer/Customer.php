<?php

namespace App\Models\Customer;

use App\Models\Account\Account;
use App\Models\Address\Address;
use App\Models\CustomerType\CustomerType;
use App\Models\Tag\Tag;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Customer.
 *
 * @package namespace App\Models\Customer;
 */
class Customer extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'customer_type_id',
        'first_name',
        'last_name',
        'mobile_phone',
        'home_phone',
        'work_phone',
        'email',
        'company',
        'team',
        'job_title',
        'billing_method',
        'price',
        'business_type',
        'receive_notifications'
    ];

    /**
     * Get all of the tags for the customer.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get all of the video's comments.
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customerType()
    {
        return $this->belongsTo(CustomerType::class);
    }
}
