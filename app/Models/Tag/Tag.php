<?php

namespace App\Models\Tag;

use App\Models\Customer\Customer;
use App\Models\Service\Service;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Tag.
 *
 * @package namespace App\Models\Tag;
 */
class Tag extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'name'
    ];

    /**
     * Get all of the employees that are assigned this tag.
     */
    public function employees()
    {
        return $this->morphedByMany(User::class, 'taggable');
    }

    /**
     * Get all of the customers that are assigned this tag.
     */
    public function customers()
    {
        return $this->morphedByMany(Customer::class, 'taggable');
    }

    /**
     * Get all of the services that are assigned this tag.
     */
    public function services()
    {
        return $this->morphedByMany(Service::class, 'taggable');
    }
}

Relation::morphMap([
    'users' => 'App\Models\User\User',
    'customers' => 'App\Models\Customer\Customer',
    'services' => 'App\Models\Service\Service',
]);
