<?php

namespace App\Models\Account;

use App\Models\AccountsAddons\AccountsAddons;
use App\Models\AccountsNotifications\AccountsNotifications;
use App\Models\AccountsPlans\AccountsPlans;
use App\Models\AccountsProperty\AccountsProperty;
use App\Models\Address\Address;
use App\Models\BusinessHour\BusinessHour;
use App\Models\Customer\Customer;
use App\Models\Industry\Industry;
use App\Models\Service\Service;
use App\Models\Setting\Setting;
use App\Models\User\User;
use App\Models\ZipCode\ZipCode;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Account.
 *
 * @package namespace App\Models\Account;
 */
class Account extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_name',
        'business_phone',
        'business_website',
        'business_profile',
        'business_logo',
        'support_email'
    ];

    /**
     * Get all of the video's comments.
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(AccountsNotifications::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class,'accounts_services')
            ->withPivot([
                'price', 'cost', 'unit', 'is_taxable', 'book_online', 'allow_consumer_app'
            ])
            ->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function industries()
    {
        return $this->belongsToMany(Service::class,'accounts_industries');
    }

    /**
     * Get the services for the category.
     */
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businessHours()
    {
        return $this->hasMany(BusinessHour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function settings()
    {
        return $this->belongsToMany(Setting::class,'accounts_settings')
            ->withPivot([
                'value'
            ])
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addons()
    {
        return $this->hasMany(AccountsAddons::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function plans()
    {
        return $this->hasMany(AccountsPlans::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany(AccountsProperty::class);
    }
}
