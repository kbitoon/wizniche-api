<?php

namespace App\Models\Role;

use App\Models\Permission\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Role.
 *
 * @package namespace App\Models\Role;
 */
class Role extends Model implements Transformable
{
    use TransformableTrait;

    const ROLE_SUPER_ADMINISTRATOR = 'super-administrator';
    const ROLE_SUBSCRIBER = 'subscriber';
    const ROLE_TECHNICIAN = 'technician';
    const ROLE_ADMINISTRATOR = 'administrator';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions() {
        return $this->belongsToMany(Permission::class,'roles_permissions');
    }

    /**
     * @param $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] =  Str::slug($name);;
    }
}
