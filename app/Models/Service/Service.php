<?php

namespace App\Models\Service;

use App\Models\Account\Account;
use App\Models\Category\Category;
use App\Models\Tag\Tag;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Service.
 *
 * @package namespace App\Models\Service;
 */
class Service extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'service_id',
        'name',
        'description',
        'price',
        'cost',
        'unit',
        'estimate_doc'
    ];

    /**
     * Get all of the tags for the service.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accounts()
    {
        return $this->belongsToMany(Account::class,'accounts_services')
            ->withPivot([
                'price', 'cost', 'unit', 'is_taxable', 'book_online', 'allow_consumer_app'
            ])
            ->withTimestamps();
    }

    /**
     * @return mixed
     */
    public function employees()
    {
        return $this->belongsToMany(User::class,'employees_services');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Service::class, 'service_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Service::class, 'id', 'service_id');
    }
}
