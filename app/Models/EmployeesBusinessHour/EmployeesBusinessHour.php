<?php

namespace App\Models\EmployeesBusinessHour;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmployeesBusinessHour.
 *
 * @package namespace App\Models\EmployeesBusinessHour;
 */
class EmployeesBusinessHour extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'day',
        'start_time',
        'end_time'
    ];

    /**
     * @return mixed
     */
    public function employee()
    {
        return $this->belongsTo(User::class);
    }

}
