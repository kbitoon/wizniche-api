<?php

namespace App\Models\Address;

use App\Models\Account\Account;
use App\Models\AccountsProperty\AccountsProperty;
use App\Models\Customer\Customer;
use App\Models\User\User;
use App\Models\ZipCode\ZipCode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Address.
 *
 * @package namespace App\Models\Address;
 */
class Address extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'addressable_id',
        'addressable_type',
        'street',
        'unit',
        'city',
        'state',
        'zip',
        'address_notes',
        'latitude',
        'longitude',
        'is_primary',
        'is_billing',
        'has_property_information'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
//    public function addressable()
//    {
//        return $this->morphTo();
//    }

    /**
     * @return mixed
     */
    public function accounts()
    {
        return $this->belongsTo(Account::class, 'addressable_id')
            ->whereAddressableType('accounts');
    }

    /**
     * @return mixed
     */
    public function customers()
    {
        return $this->belongsTo(Customer::class, 'addressable_id')
            ->whereAddressableType('customers');
    }

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'addressable_id')
            ->whereAddressableType('users');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function accountProperty()
    {
        return $this->hasOne(AccountsProperty::class);
    }
}

Relation::morphMap([
    'users' => 'App\Models\User\User',
    'customers' => 'App\Models\Customer\Customer',
    'accounts' => 'App\Models\Account\Account',
]);
