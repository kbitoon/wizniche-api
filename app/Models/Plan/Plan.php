<?php

namespace App\Models\Plan;

use App\Models\Addon\Addon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Plan.
 *
 * @package namespace App\Models\Plan;
 */
class Plan extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'price',
        'max_users',
        'is_active'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function addons()
    {
        return $this->belongsToMany(Addon::class,'plans_addons')
            ->withPivot([
                'added_at', 'removed_at'
            ])
            ->withTimestamps();
    }
}
