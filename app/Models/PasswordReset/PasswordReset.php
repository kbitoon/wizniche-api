<?php

namespace App\Models\PasswordReset;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PasswordReset.
 *
 * @package namespace App\Models\PasswordReset;
 */
class PasswordReset extends Model implements Transformable
{
    use TransformableTrait;

    public $primaryKey = 'email';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'token'];

    public function getEmailForPasswordReset()
    {
        return $this->email;
    }
}
