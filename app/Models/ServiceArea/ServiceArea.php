<?php

namespace App\Models\ServiceArea;

use App\Models\Account\Account;
use App\Models\Address\Address;
use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use App\Models\ZipCode\ZipCode;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ServiceArea.
 *
 * @package namespace App\Models\ServiceArea;
 */
class ServiceArea extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'address_id',
        'type',
        'mile_radius',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(ServiceAreasDetail::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zipCodes()
    {
        return $this->hasMany(ZipCode::class);
    }
}
