<?php

namespace App\Models\BusinessHour;

use App\Models\Account\Account;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class BusinessHour.
 *
 * @package namespace App\Models\BusinessHour;
 */
class BusinessHour extends Model implements Transformable
{
    use TransformableTrait;

    const BUSINESS_HOURS_OPEN_CLOSE = 1;
    const BUSINESS_ONLINE_BOOKING_WINDOW = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'type',
        'monday_in',
        'monday_out',
        'tuesday_in',
        'tuesday_out',
        'wednesday_in',
        'wednesday_out',
        'thursday_in',
        'thursday_out',
        'friday_in',
        'friday_out',
        'saturday_in',
        'saturday_out',
        'sunday_in',
        'sunday_out'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
