<?php

namespace App\Models\AccountsProperty;

use App\Models\Account\Account;
use App\Models\Address\Address;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AccountsProperty.
 *
 * @package namespace App\Models\AccountsProperty;
 */
class AccountsProperty extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'address_id',
        'address',
        'city',
        'state',
        'zip',
        'mak',
        'base_mak',
        'address_key',
        'latitude',
        'longitude',
        'formatted_apn',
        'county',
        'fips_code',
        'census_track',
        'census_block',
        'owner_address',
        'owner_city',
        'owner_state',
        'owner_zip',
        'owner_mak',
        'owner_type',
        'owner_name_one_full',
        'owner_name_two_full',
        'estimated_value',
        'year_assessed',
        'assessed_value_total',
        'assessed_value_land',
        'assessed_value_improvements',
        'tax_billed_amount',
        'year_built',
        'zoned_code_local',
        'area_building',
        'area_first_floor',
        'area_second_floor',
        'area_lot_acres',
        'area_lot_sf',
        'parking_garage_area',
        'bedrooms_count',
        'bath_count',
        'results'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
