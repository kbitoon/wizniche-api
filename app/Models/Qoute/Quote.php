<?php

namespace App\Models\Qoute;

use App\Models\QuotesSchedule\QuotesSchedule;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Quote.
 *
 * @package namespace App\Models\Qoute;
 */
class Quote extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'author',
        'quote'
    ];

    /**
     * @return mixed
     */
    public function quotesSchedules()
    {
        return $this->belongsToMany(QuotesSchedule::class,'quotes_quotes_schedules');
    }
}
