<?php

namespace App\Models\Position;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Position.
 *
 * @package namespace App\Models\Position;
 */
class Position extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'name'
    ];

    /**
     * @return mixed
     */
    public function employees()
    {
        return $this->belongsToMany(User::class,'employees_positions');
    }
}
