<?php

namespace App\Models\ServiceAreaDetail;

use App\Models\ServiceArea\ServiceArea;
use App\Models\User\User;
use App\Models\ZipCode\ZipCode;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ServiceAreasDetail.
 *
 * @package namespace App\Models\ServiceAreasDetail;
 */
class ServiceAreasDetail extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_area_id',
        'zone_name',
        'is_available_monday',
        'is_available_tuesday',
        'is_available_wednesday',
        'is_available_thursday',
        'is_available_friday',
        'is_available_saturday',
        'is_available_sunday'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceArea()
    {
        return $this->belongsTo(ServiceArea::class);
    }

    /**
     * @return mixed
     */
    public function zipCodes()
    {
        return $this->belongsToMany(ZipCode::class,'service_areas_details_zip_codes');
    }

    /**
     * @return mixed
     */
    public function technicians()
    {
        return $this->belongsToMany(User::class,'service_areas_details_technicians');
    }
}
