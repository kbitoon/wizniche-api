<?php

namespace App\Models\AccountsNotifications;

use App\Models\Account\Account;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class AccountsNotifications.
 *
 * @package namespace App\Models\AccountsNotifications;
 */
class AccountsNotifications extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'group',
        'slug',
        'name',
        'notification_type',
        'context',
        'is_enabled'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
