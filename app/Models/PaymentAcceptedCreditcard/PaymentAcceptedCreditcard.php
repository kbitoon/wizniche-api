<?php

namespace App\Models\PaymentAcceptedCreditcard;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class PaymentAcceptedCreditcard.
 *
 * @package namespace App\Models\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcard extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code'
    ];

}
