<?php

namespace App\Models\ZipCode;

use App\Models\Account\Account;
use App\Models\Address\Address;
use App\Models\ServiceArea\ServiceArea;
use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ZipCode.
 *
 * @package namespace App\Models\ZipCode;
 */
class ZipCode extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_area_id',
        'code',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function serviceArea()
    {
        return $this->belongsTo(ServiceArea::class);
    }

    /**
     * @return mixed
     */
    public function serviceAreasDetails()
    {
        return $this->belongsToMany(ServiceAreasDetail::class,'service_areas_details_zip_codes');
    }
}
