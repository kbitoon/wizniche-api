<?php

namespace App\Models\User;

use App\Models\Account\Account;
use App\Models\Address\Address;
use App\Models\EmployeesBusinessHour\EmployeesBusinessHour;
use App\Models\EmployeesCertification\EmployeesCertification;
use App\Models\Position\Position;
use App\Models\Service\Service;
use App\Models\ServiceAreaDetail\ServiceAreasDetail;
use App\Models\Tag\Tag;
use App\Traits\Permissions\HasPermissionsTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements Transformable, MustVerifyEmail
{
    use HasApiTokens, Notifiable, HasPermissionsTrait, TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'first_name',
        'last_name',
        'mobile_number',
        'is_account_owner',
        'email',
        'password',
        'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Automatically hash password for user
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] =  bcrypt($password);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        $this->attributes['full_name'] = ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    /**
     * @return array
     */
    public function getScopeAttribute()
    {
        $roles = [];
        foreach ($this->roles as $role) {
            $roles[] = $role->slug;
        }
        return $roles;
    }

    /**
     * Get all of the tags for the employee.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get all of the video's comments.
     */
    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * @return mixed
     */
    public function serviceAreasDetails()
    {
        return $this->belongsToMany(ServiceAreasDetail::class,'service_areas_details_technicians');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function certifications()
    {
        return $this->hasMany(EmployeesCertification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'employees_services');
    }

    /**
     * @return mixed
     */
    public function businessHours()
    {
        return $this->hasMany(EmployeesBusinessHour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function positions()
    {
        return $this->belongsToMany(Position::class, 'employees_positions');
    }
}
