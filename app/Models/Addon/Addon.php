<?php

namespace App\Models\Addon;

use App\Models\Plan\Plan;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Addon.
 *
 * @package namespace App\Models\Addon;
 */
class Addon extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'price'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function plans()
    {
        return $this->belongsToMany(Plan::class,'plans_addons')
            ->withPivot([
                'added_at', 'removed_at'
            ])
            ->withTimestamps();
    }
}
