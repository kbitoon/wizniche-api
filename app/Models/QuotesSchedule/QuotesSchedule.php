<?php

namespace App\Models\QuotesSchedule;

use App\Models\Qoute\Quote;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class QuotesSchedule.
 *
 * @package namespace App\Models\QuotesSchedule;
 */
class QuotesSchedule extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'start_date',
        'end_date'
    ];

    /**
     * @return mixed
     */
    public function quotes()
    {
        return $this->belongsToMany(Quote::class,'quotes_quotes_schedules');
    }
}
