<?php

namespace App\Transformers\Setting;

use App\Transformers\Account\AccountTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Setting\Setting;

/**
 * Class SettingTransformer.
 *
 * @package namespace App\Transformers\Setting;
 */
class SettingTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'accounts'
    ];

    /**
     * Transform the Setting entity.
     *
     * @param \App\Models\Setting\Setting $model
     *
     * @return array
     */
    public function transform(Setting $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'data_type' => $model->data_type,
            'slug' => $model->slug,
            'name' => $model->name,
            'description' => $model->description,
            'min_value' => $model->min_value,
            'max_value' => $model->max_value,
            'options' => $model->options,
            'default_value' => $model->default_value,
        ];
    }

    /**
     * @param Setting $setting
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAccounts(Setting $setting)
    {
        return $this->collection($setting->accounts, new AccountTransformer());
    }
}
