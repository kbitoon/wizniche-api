<?php

namespace App\Transformers\PasswordReset;

use League\Fractal\TransformerAbstract;
use App\Models\PasswordReset\PasswordReset;

/**
 * Class PasswordResetTransformer.
 *
 * @package namespace App\Transformers\PasswordReset;
 */
class PasswordResetTransformer extends TransformerAbstract
{
    /**
     * Transform the PasswordReset entity.
     *
     * @param \App\Models\PasswordReset\PasswordReset $model
     *
     * @return array
     */
    public function transform(PasswordReset $model)
    {
        return [
//            'id'         => (int) $model->id,

            /* place your other model properties here */
            'token' => $model->token,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
