<?php

namespace App\Transformers\Tag;

use League\Fractal\TransformerAbstract;
use App\Models\Tag\Tag;

/**
 * Class TagTransformer.
 *
 * @package namespace App\Transformers\Tag;
 */
class TagTransformer extends TransformerAbstract
{
    /**
     * Transform the Tag entity.
     *
     * @param \App\Models\Tag\Tag $model
     *
     * @return array
     */
    public function transform(Tag $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'name' => $model->name
        ];
    }
}
