<?php

namespace App\Transformers\Role;

use App\Transformers\Permission\PermissionTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Role\Role;

/**
 * Class RoleTransformer.
 *
 * @package namespace App\Transformers\Role;
 */
class RoleTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'permissions'
    ];

    /**
     * Transform the Role entity.
     *
     * @param \App\Models\Role\Role $model
     *
     * @return array
     */
    public function transform(Role $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name
        ];
    }

    /**
     * @param Role $role
     * @return \League\Fractal\Resource\Collection
     */
    public function includePermissions(Role $role)
    {
        return $this->collection($role->permissions, new PermissionTransformer());
    }
}
