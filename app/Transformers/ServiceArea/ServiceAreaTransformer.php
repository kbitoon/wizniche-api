<?php

namespace App\Transformers\ServiceArea;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Address\AddressTransformer;
use App\Transformers\ServiceAreaDetail\ServiceAreasDetailTransformer;
use App\Transformers\ZipCode\ZipCodeTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\ServiceArea\ServiceArea;

/**
 * Class ServiceAreaTransformer.
 *
 * @package namespace App\Transformers\ServiceArea;
 */
class ServiceAreaTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'details',
        'account',
        'address',
        'zipCodes'
    ];

    /**
     * Transform the ServiceArea entity.
     *
     * @param \App\Models\ServiceArea\ServiceArea $model
     *
     * @return array
     */
    public function transform(ServiceArea $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'address_id' => $model->address_id,
            'type' => $model->type,
            'mile_radius' => $model->mile_radius
        ];
    }

    /**
     * @param ServiceArea $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeDetails(ServiceArea $model)
    {
        return $this->collection($model->details, new ServiceAreasDetailTransformer());
    }

    /**
     * @param ServiceArea $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(ServiceArea $model)
    {
        return $this->item($model->account, new AccountTransformer());
    }

    /**
     * @param ServiceArea $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddress(ServiceArea $model)
    {
        return $this->item($model->address, new AddressTransformer());
    }

    /**
     * @param ServiceArea $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeZipCodes(ServiceArea $model)
    {
        return $this->collection($model->zipCodes, new ZipCodeTransformer());
    }
}
