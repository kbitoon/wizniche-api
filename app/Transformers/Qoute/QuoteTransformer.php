<?php

namespace App\Transformers\Qoute;

use App\Transformers\QuotesSchedule\QuotesScheduleTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Qoute\Quote;

/**
 * Class QuoteTransformer.
 *
 * @package namespace App\Transformers\Qoute;
 */
class QuoteTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'quotesSchedules'
    ];

    /**
     * Transform the Quote entity.
     *
     * @param \App\Models\Qoute\Quote $model
     *
     * @return array
     */
    public function transform(Quote $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'author' => $model->author,
            'quote' => $model->quote
        ];
    }

    /**
     * @param Quote $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeQuotesSchedules(Quote $model)
    {
        return $this->collection($model->quotesSchedules, new QuotesScheduleTransformer());
    }
}
