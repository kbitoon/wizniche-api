<?php

namespace App\Transformers\EmployeesBusinessHour;

use League\Fractal\TransformerAbstract;
use App\Models\EmployeesBusinessHour\EmployeesBusinessHour;

/**
 * Class EmployeesBusinessHourTransformer.
 *
 * @package namespace App\Transformers\EmployeesBusinessHour;
 */
class EmployeesBusinessHourTransformer extends TransformerAbstract
{
    /**
     * Transform the EmployeesBusinessHour entity.
     *
     * @param \App\Models\EmployeesBusinessHour\EmployeesBusinessHour $model
     *
     * @return array
     */
    public function transform(EmployeesBusinessHour $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'user_id' => $model->user_id,
            'day' => $model->day,
            'start_time' => $model->start_time,
            'end_time' => $model->end_time
        ];
    }
}
