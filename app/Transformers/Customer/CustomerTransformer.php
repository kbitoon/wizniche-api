<?php

namespace App\Transformers\Customer;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Address\AddressTransformer;
use App\Transformers\CustomerType\CustomerTypeTransformer;
use App\Transformers\Tag\TagTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Customer\Customer;

/**
 * Class CustomerTransformer.
 *
 * @package namespace App\Transformers\Customer;
 */
class CustomerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'tags',
        'addresses',
        'account',
        'customerType',
        'tags'
    ];

    /**
     * Transform the Customer entity.
     *
     * @param \App\Models\Customer\Customer $model
     *
     * @return array
     */
    public function transform(Customer $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'customer_type_id' => $model->customer_type_id,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'mobile_phone' => $model->mobile_phone,
            'home_phone' => $model->home_phone,
            'work_phone' => $model->work_phone,
            'email' => $model->email,
            'company' => $model->company,
            'team' => $model->team,
            'job_title' => $model->job_title,
            'billing_method' => $model->billing_method,
            'price' => $model->price,
            'business_type' => $model->business_type,
            'receive_notifications' => $model->receive_notifications
        ];
    }

    /**
     * @param Customer $customer
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTags(Customer $customer)
    {
        return $this->collection($customer->tags, new TagTransformer());
    }

    /**
     * @param Customer $customer
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAddresses(Customer $customer)
    {
        return $this->collection($customer->addresses, new AddressTransformer());
    }

    /**
     * @param Customer $customer
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(Customer $customer)
    {
        return $this->item($customer->account, new AccountTransformer());
    }

    /**
     * @param Customer $customer
     * @return \League\Fractal\Resource\Item
     */
    public function includeCustomerType(Customer $customer)
    {
        return $this->item($customer->customerType, new CustomerTypeTransformer());
    }
}
