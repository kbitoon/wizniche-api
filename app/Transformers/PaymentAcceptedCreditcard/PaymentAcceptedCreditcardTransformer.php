<?php

namespace App\Transformers\PaymentAcceptedCreditcard;

use League\Fractal\TransformerAbstract;
use App\Models\PaymentAcceptedCreditcard\PaymentAcceptedCreditcard;

/**
 * Class PaymentAcceptedCreditcardTransformer.
 *
 * @package namespace App\Transformers\PaymentAcceptedCreditcard;
 */
class PaymentAcceptedCreditcardTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentAcceptedCreditcard entity.
     *
     * @param \App\Models\PaymentAcceptedCreditcard\PaymentAcceptedCreditcard $model
     *
     * @return array
     */
    public function transform(PaymentAcceptedCreditcard $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
