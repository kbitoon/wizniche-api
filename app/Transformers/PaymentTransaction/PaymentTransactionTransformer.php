<?php

namespace App\Transformers\PaymentTransaction;

use League\Fractal\TransformerAbstract;
use App\Models\PaymentTransaction\PaymentTransaction;

/**
 * Class PaymentTransactionTransformer.
 *
 * @package namespace App\Transformers\PaymentTransaction;
 */
class PaymentTransactionTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentTransaction entity.
     *
     * @param \App\Models\PaymentTransaction\PaymentTransaction $model
     *
     * @return array
     */
    public function transform(PaymentTransaction $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
