<?php

namespace App\Transformers\PlanAddon;

use App\Models\Addon\Addon;
use League\Fractal\TransformerAbstract;

/**
 * Class PlanAddonTransformer.
 *
 * @package namespace App\Transformers\Setting;
 */
class PlanAddonTransformer extends TransformerAbstract
{
    /**
     * Transform the Setting entity.
     *
     * @param Addon $model
     *
     * @return array
     */
    public function transform(Addon $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'description' => $model->description,
            'price' => $model->price,
            'added_at' => $model->pivot->added_at,
            'removed_at' => $model->pivot->removed_at
        ];
    }
}
