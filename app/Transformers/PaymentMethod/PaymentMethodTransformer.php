<?php

namespace App\Transformers\PaymentMethod;

use League\Fractal\TransformerAbstract;
use App\Models\PaymentMethod\PaymentMethod;

/**
 * Class PaymentMethodTransformer.
 *
 * @package namespace App\Transformers\PaymentMethod;
 */
class PaymentMethodTransformer extends TransformerAbstract
{
    /**
     * Transform the PaymentMethod entity.
     *
     * @param \App\Models\PaymentMethod\PaymentMethod $model
     *
     * @return array
     */
    public function transform(PaymentMethod $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
