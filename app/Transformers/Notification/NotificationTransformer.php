<?php

namespace App\Transformers\Notification;

use League\Fractal\TransformerAbstract;
use App\Models\Notification\Notification;

/**
 * Class NotificationTransformer.
 *
 * @package namespace App\Transformers\Notification;
 */
class NotificationTransformer extends TransformerAbstract
{
    /**
     * Transform the Notification entity.
     *
     * @param \App\Models\Notification\Notification $model
     *
     * @return array
     */
    public function transform(Notification $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
