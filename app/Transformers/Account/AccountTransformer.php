<?php

namespace App\Transformers\Account;

use App\Transformers\AccountsAddons\AccountsAddonsTransformer;
use App\Transformers\AccountIndustry\AccountIndustryTransformer;
use App\Transformers\AccountService\AccountServiceTransformer;
use App\Transformers\AccountSetting\AccountSettingTransformer;
use App\Transformers\AccountsNotifications\AccountsNotificationsTransformer;
use App\Transformers\AccountsPlans\AccountsPlansTransformer;
use App\Transformers\Address\AddressTransformer;
use App\Transformers\BusinessHour\BusinessHourTransformer;
use App\Transformers\Customer\CustomerTransformer;
use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Account\Account;

/**
 * Class AccountTransformer.
 *
 * @package namespace App\Transformers\Account;
 */
class AccountTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'addresses',
        'businessHours',
        'customers',
        'employees',
        'industries',
        'services',
        'settings',
        'addons',
        'plans',
        'notifications'
    ];

    /**
     * Transform the Account entity.
     *
     * @param \App\Models\Account\Account $model
     *
     * @return array
     */
    public function transform(Account $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'business_name' => $model->business_name,
            'business_phone' => $model->business_phone,
            'business_website' => $model->business_website,
            'business_profile' => $model->business_profile,
            'support_email' => $model->support_email,
        ];
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAddresses(Account $account)
    {
        return $this->collection($account->addresses, new AddressTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeEmployees(Account $account)
    {
        return $this->collection($account->employees, new UserTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeServices(Account $account)
    {
        return $this->collection($account->services, new AccountServiceTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeIndustries(Account $account)
    {
        return $this->collection($account->industries, new AccountIndustryTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCustomers(Account $account)
    {
        return $this->collection($account->customers, new CustomerTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBusinessHours(Account $account)
    {
        return $this->collection($account->businessHours, new BusinessHourTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeSettings(Account $account)
    {
        return $this->collection($account->settings, new AccountSettingTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAddons(Account $account)
    {
        return $this->collection($account->addons, new AccountsAddonsTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includePlans(Account $account)
    {
        return $this->collection($account->plans, new AccountsPlansTransformer());
    }

    /**
     * @param Account $account
     * @return \League\Fractal\Resource\Collection
     */
    public function includeNotifications(Account $account)
    {
        return $this->collection($account->notifications, new AccountsNotificationsTransformer());
    }
}
