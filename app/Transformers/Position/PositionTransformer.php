<?php

namespace App\Transformers\Position;

use League\Fractal\TransformerAbstract;
use App\Models\Position\Position;

/**
 * Class PositionTransformer.
 *
 * @package namespace App\Transformers\Position;
 */
class PositionTransformer extends TransformerAbstract
{
    /**
     * Transform the Position entity.
     *
     * @param \App\Models\Position\Position $model
     *
     * @return array
     */
    public function transform(Position $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'name' => $model->name
        ];
    }
}
