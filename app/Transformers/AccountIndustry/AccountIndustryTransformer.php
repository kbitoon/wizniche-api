<?php

namespace App\Transformers\AccountIndustry;

use App\Models\Service\Service;
use App\Transformers\Account\AccountTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ServiceTransformer.
 *
 * @package namespace App\Transformers\Service;
 */
class AccountIndustryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'accounts',
        'categories'
    ];

    /**
     * Transform the Service entity.
     *
     * @param Service $model
     *
     * @return array
     */
    public function transform(Service $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
        ];
    }

    /**
     * @param Service $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAccounts(Service $model)
    {
        return $this->collection($model->accounts, new AccountTransformer());
    }

}
