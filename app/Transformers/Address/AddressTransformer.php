<?php

namespace App\Transformers\Address;

use League\Fractal\TransformerAbstract;
use App\Models\Address\Address;

/**
 * Class AddressTransformer.
 *
 * @package namespace App\Transformers\Address;
 */
class AddressTransformer extends TransformerAbstract
{
    /**
     * Transform the Address entity.
     *
     * @param \App\Models\Address\Address $model
     *
     * @return array
     */
    public function transform(Address $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'street' => $model->street,
            'unit' => $model->unit,
            'city' => $model->city,
            'state' => $model->state,
            'zip' => $model->zip,
            'address_notes' => $model->address_notes,
            'latitude' => $model->latitude,
            'longitude' => $model->longitude,
            'is_primary' => $model->is_primary,
            'is_billing' => $model->is_billing,
            'has_property_information' => $model->has_property_information
        ];
    }
}
