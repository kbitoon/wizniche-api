<?php

namespace App\Transformers\CustomerType;

use League\Fractal\TransformerAbstract;
use App\Models\CustomerType\CustomerType;

/**
 * Class CustomerTypeTransformer.
 *
 * @package namespace App\Transformers\CustomerType;
 */
class CustomerTypeTransformer extends TransformerAbstract
{
    /**
     * Transform the CustomerType entity.
     *
     * @param \App\Models\CustomerType\CustomerType $model
     *
     * @return array
     */
    public function transform(CustomerType $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name
        ];
    }
}
