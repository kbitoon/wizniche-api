<?php

namespace App\Transformers\BusinessHour;

use App\Transformers\Account\AccountTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\BusinessHour\BusinessHour;

/**
 * Class BusinessHourTransformer.
 *
 * @package namespace App\Transformers\BusinessHour;
 */
class BusinessHourTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'account'
    ];

    /**
     * Transform the BusinessHour entity.
     *
     * @param \App\Models\BusinessHour\BusinessHour $model
     *
     * @return array
     */
    public function transform(BusinessHour $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'type' => $model->type,
            'monday_in' => $model->monday_in,
            'monday_out' => $model->monday_out,
            'tuesday_in' => $model->tuesday_in,
            'tuesday_out' => $model->tuesday_out,
            'wednesday_in' => $model->wednesday_in,
            'wednesday_out' => $model->wednesday_out,
            'thursday_in' => $model->thursday_in,
            'thursday_out' => $model->thursday_out,
            'friday_in' => $model->friday_in,
            'friday_out' => $model->friday_out,
            'saturday_in' => $model->saturday_in,
            'saturday_out' => $model->saturday_out,
            'sunday_in' => $model->sunday_in,
            'sunday_out' => $model->sunday_out
        ];
    }

    /**
     * @param BusinessHour $businessHour
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(BusinessHour $businessHour)
    {
        return $this->item($businessHour->account, new AccountTransformer());
    }
}
