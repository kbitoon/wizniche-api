<?php

namespace App\Transformers\Plan;

use App\Transformers\PlanAddon\PlanAddonTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Plan\Plan;

/**
 * Class PlanTransformer.
 *
 * @package namespace App\Transformers\Plan;
 */
class PlanTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'addons',
    ];

    /**
     * Transform the Plan entity.
     *
     * @param \App\Models\Plan\Plan $model
     *
     * @return array
     */
    public function transform(Plan $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'description' => $model->description,
            'max_users' => $model->max_users,
            'price' => $model->price,
            'is_active' => $model->is_active
        ];
    }

    /**
     * @param Plan $plan
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAddons(Plan $plan)
    {
        return $this->collection($plan->addons, new PlanAddonTransformer());
    }
}
