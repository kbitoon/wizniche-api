<?php

namespace App\Transformers\User;

use App\Transformers\Address\AddressTransformer;
use App\Transformers\EmployeesBusinessHour\EmployeesBusinessHourTransformer;
use App\Transformers\EmployeesCertification\EmployeesCertificationTransformer;
use App\Transformers\Permission\PermissionTransformer;
use App\Transformers\Position\PositionTransformer;
use App\Transformers\Role\RoleTransformer;
use App\Transformers\Service\ServiceTransformer;
use App\Transformers\ServiceAreaDetail\ServiceAreasDetailTransformer;
use App\Transformers\Tag\TagTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\User\User;

/**
 * Class UserTransformer.
 *
 * @package namespace App\Transformers\User;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
        'permissions',
        'tags',
        'serviceAreasDetails',
        'addresses',
        'certifications',
        'services',
        'businessHours',
        'positions'
    ];

    protected $defaultIncludes = [

    ];

    /**
     * Transform the User entity.
     *
     * @param \App\Models\User\User $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'first_name' => $model->first_name,
            'last_name' => $model->last_name,
            'mobile_number' => $model->mobile_number,
            'email' => $model->email
        ];
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRoles(User $user)
    {
        return $this->collection($user->roles, new RoleTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includePermissions(User $user)
    {
        return $this->collection($user->permissions, new PermissionTransformer());
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTags(User $user)
    {
        return $this->collection($user->tags, new TagTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeServiceAreasDetails(User $model)
    {
        return $this->collection($model->serviceAreasDetails, new ServiceAreasDetailTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAddresses(User $model)
    {
        return $this->collection($model->addresses, new AddressTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCertifications(User $model)
    {
        return $this->collection($model->certifications, new EmployeesCertificationTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeServices(User $model)
    {
        return $this->collection($model->services, new ServiceTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBusinessHours(User $model)
    {
        return $this->collection($model->businessHours, new EmployeesBusinessHourTransformer());
    }

    /**
     * @param User $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includePositions(User $model)
    {
        return $this->collection($model->positions, new PositionTransformer());
    }
}
