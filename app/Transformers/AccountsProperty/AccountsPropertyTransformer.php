<?php

namespace App\Transformers\AccountsProperty;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Address\AddressTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\AccountsProperty\AccountsProperty;

/**
 * Class AccountsPropertyTransformer.
 *
 * @package namespace App\Transformers\AccountsProperty;
 */
class AccountsPropertyTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'address',
        'account'
    ];

    /**
     * Transform the AccountsProperty entity.
     *
     * @param \App\Models\AccountsProperty\AccountsProperty $model
     *
     * @return array
     */
    public function transform(AccountsProperty $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'address_id' => $model->address_id,
            'address' => $model->address,
            'city' => $model->city,
            'state' => $model->state,
            'zip' => $model->zip,
            'mak' => $model->mak,
            'base_mak' => $model->base_mak,
            'address_key' => $model->address_key,
            'latitude' => $model->latitude,
            'longitude' => $model->longitude,
            'formatted_apn' => $model->formatted_apn,
            'county' => $model->county,
            'fips_code' => $model->fips_code,
            'census_track' => $model->census_track,
            'census_block' => $model->census_block,
            'owner_address' => $model->owner_address,
            'owner_city' => $model->owner_city,
            'owner_state' => $model->owner_state,
            'owner_zip' => $model->owner_zip,
            'owner_mak' => $model->owner_mak,
            'owner_type' => $model->owner_type,
            'owner_name_one_full' => $model->owner_name_one_full,
            'owner_name_two_full' => $model->owner_name_two_full,
            'estimated_value' => $model->estimated_value,
            'year_assessed' => $model->year_assessed,
            'assessed_value_total' => $model->assessed_value_total,
            'assessed_value_land' => $model->assessed_value_land,
            'assessed_value_improvements' => $model->assessed_value_improvements,
            'tax_billed_amount' => $model->tax_billed_amount,
            'year_built' => $model->year_built,
            'zoned_code_local' => $model->zoned_code_local,
            'area_building' => $model->area_building,
            'area_first_floor' => $model->area_first_floor,
            'area_second_floor' => $model->area_second_floor,
            'area_lot_acres' => $model->area_lot_acres,
            'area_lot_sf' => $model->area_lot_sf,
            'parking_garage_area' => $model->parking_garage_area,
            'bedrooms_count' => $model->bedrooms_count,
            'bath_count' => $model->bath_count,
            'results' => $model->results,

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    /**
     * @param AccountsProperty $model
     * @return \League\Fractal\Resource\Item
     */
    public function includePlan(AccountsProperty $model)
    {
        return $this->item($model->address, new AddressTransformer());
    }

    /**
     * @param AccountsProperty $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(AccountsProperty $model)
    {
        return $this->item($model->account, new AccountTransformer());
    }
}
