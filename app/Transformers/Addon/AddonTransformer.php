<?php

namespace App\Transformers\Addon;

use App\Transformers\Plan\PlanTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Addon\Addon;

/**
 * Class AddonTransformer.
 *
 * @package namespace App\Transformers\Addon;
 */
class AddonTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'plans',
    ];

    /**
     * Transform the Addon entity.
     *
     * @param \App\Models\Addon\Addon $model
     *
     * @return array
     */
    public function transform(Addon $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'description' => $model->description,
            'price' => $model->price
        ];
    }

    /**
     * @param Addon $addon
     * @return \League\Fractal\Resource\Collection
     */
    public function includePlans(Addon $addon)
    {
        return $this->collection($addon->plans, new PlanTransformer());
    }
}
