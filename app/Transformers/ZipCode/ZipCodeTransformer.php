<?php

namespace App\Transformers\ZipCode;

use App\Models\ServiceArea\ServiceArea;
use App\Transformers\Account\AccountTransformer;
use App\Transformers\Address\AddressTransformer;
use App\Transformers\ServiceArea\ServiceAreaTransformer;
use App\Transformers\ServiceAreaDetail\ServiceAreasDetailTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\ZipCode\ZipCode;

/**
 * Class ZipCodeTransformer.
 *
 * @package namespace App\Transformers\ZipCode;
 */
class ZipCodeTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'serviceArea',
        'serviceAreasDetails'
    ];

    /**
     * Transform the ZipCode entity.
     *
     * @param \App\Models\ZipCode\ZipCode $model
     *
     * @return array
     */
    public function transform(ZipCode $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'service_area_id' => $model->service_area_id,
            'code' => $model->code,
        ];
    }

    /**
     * @param ZipCode $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeServiceArea(ZipCode $model)
    {
        return $this->item($model->serviceArea, new ServiceAreaTransformer());
    }

    /**
     * @param ZipCode $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeServiceAreasDetails(ZipCode $model)
    {
        return $this->collection($model->serviceAreasDetails, new ServiceAreasDetailTransformer());
    }
}
