<?php

namespace App\Transformers\EmployeesCertification;

use App\Transformers\User\UserTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\EmployeesCertification\EmployeesCertification;

/**
 * Class EmployeesCertificationTransformer.
 *
 * @package namespace App\Transformers\EmployeesCertification;
 */
class EmployeesCertificationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'employee'
    ];

    /**
     * Transform the EmployeesCertification entity.
     *
     * @param \App\Models\EmployeesCertification\EmployeesCertification $model
     *
     * @return array
     */
    public function transform(EmployeesCertification $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'user_id' => $model->user_id,
            'name' => $model->name,
        ];
    }

    /**
     * @param EmployeesCertification $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeServiceArea(EmployeesCertification $model)
    {
        return $this->item($model->employee, new UserTransformer());
    }
}
