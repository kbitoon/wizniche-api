<?php

namespace App\Transformers\Service;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Tag\TagTransformer;
use Illuminate\Http\Request;
use League\Fractal\TransformerAbstract;
use App\Models\Service\Service;

/**
 * Class ServiceTransformer.
 *
 * @package namespace App\Transformers\Service;
 */
class ServiceTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'tags',
        'accounts',
        'children'
    ];

    /**
     * Transform the Service entity.
     *
     * @param \App\Models\Service\Service $model
     *
     * @return array
     */
    public function transform(Service $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'service_id' => $model->service_id,
            'name' => $model->name,
            'description' => $model->description,
            'price' => $model->price,
            'cost' => $model->cost,
            'unit' => $model->unit,
            'is_enable' => $model->is_enable,
            'estimate_doc' => $model->estimate_doc
        ];
    }

    /**
     * @param Request $request
     */
    public function setDefaultIncludesByParams(Request $request)
    {
        $include = ($request->input('include', ''));

        $includes = explode(',', $include);
        if (in_array('allWithChildren', $includes)) {
            $this->defaultIncludes = ['allWithChildren', 'tags'];
        }
    }

    /**
     * @param Service $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTags(Service $model)
    {
        return $this->collection($model->tags, new TagTransformer());
    }

    /**
     * @param Service $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAccounts(Service $model)
    {
        return $this->collection($model->accounts, new AccountTransformer());
    }

    /**
     * @param Service $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeChildren(Service $model)
    {
         return $this->collection($model->children, new ServiceTransformer());
    }

    /**
     * @param Service $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAllWithChildren(Service $model)
    {
        return $this->collection($model->children, $this);
    }
}
