<?php

namespace App\Transformers\ServiceAreaDetail;

use App\Transformers\ServiceArea\ServiceAreaTransformer;
use App\Transformers\User\UserTransformer;
use App\Transformers\ZipCode\ZipCodeTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\ServiceAreaDetail\ServiceAreasDetail;

/**
 * Class ServiceAreasDetailTransformer.
 *
 * @package namespace App\Transformers\ServiceAreaDetail;
 */
class ServiceAreasDetailTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'serviceArea',
        'technicians',
        'zipCodes'
    ];

    /**
     * Transform the ServiceAreasDetail entity.
     *
     * @param \App\Models\ServiceAreaDetail\ServiceAreasDetail $model
     *
     * @return array
     */
    public function transform(ServiceAreasDetail $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'service_area_id' => $model->service_area_id,
            'user_id' => $model->user_id,
            'zone_name' => $model->zone_name,
            'is_available_monday' => $model->is_available_monday,
            'is_available_tuesday' => $model->is_available_tuesday,
            'is_available_wednesday' => $model->is_available_wednesday,
            'is_available_thursday' => $model->is_available_thursday,
            'is_available_friday' => $model->is_available_friday,
            'is_available_saturday' => $model->is_available_saturday,
            'is_available_sunday' => $model->is_available_sunday
        ];
    }

    /**
     * @param ServiceAreasDetail $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeServiceArea(ServiceAreasDetail $model)
    {
        return $this->item($model->serviceArea, new ServiceAreaTransformer());
    }

    /**
     * @param ServiceAreasDetail $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeZipCodes(ServiceAreasDetail $model)
    {
        return $this->collection($model->zipCodes, new ZipCodeTransformer());
    }

    /**
     * @param ServiceAreasDetail $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTechnicians(ServiceAreasDetail $model)
    {
        return $this->collection($model->technicians, new UserTransformer());
    }
}
