<?php

namespace App\Transformers\AccountsPlans;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Plan\PlanTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\AccountsPlans\AccountsPlans;

/**
 * Class AccountsPlansTransformer.
 *
 * @package namespace App\Transformers\AccountsPlans;
 */
class AccountsPlansTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'plan',
        'account'
    ];

    /**
     * Transform the AccountsPlans entity.
     *
     * @param \App\Models\AccountsPlans\AccountsPlans $model
     *
     * @return array
     */
    public function transform(AccountsPlans $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'plan_id' => $model->plan_id,
            'max_users' => $model->max_users,
            'price' => $model->price,
            'subscribed_at' => $model->subscribed_at,
            'unsubscribed_at' => $model->unsubscribed_at
        ];
    }

    /**
     * @param AccountsPlans $model
     * @return \League\Fractal\Resource\Item
     */
    public function includePlan(AccountsPlans $model)
    {
        return $this->item($model->plan, new PlanTransformer());
    }

    /**
     * @param AccountsPlans $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(AccountsPlans $model)
    {
        return $this->item($model->account, new AccountTransformer());
    }
}
