<?php

namespace App\Transformers\AccountsNotifications;

use App\Transformers\Account\AccountTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\AccountsNotifications\AccountsNotifications;

/**
 * Class AccountsNotificationsTransformer.
 *
 * @package namespace App\Transformers\AccountsNotifications;
 */
class AccountsNotificationsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'account'
    ];
    /**
     * Transform the AccountsNotifications entity.
     *
     * @param \App\Models\AccountsNotifications\AccountsNotifications $model
     *
     * @return array
     */
    public function transform(AccountsNotifications $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'group' => $model->group,
            'slug' => $model->slug,
            'name' => $model->name,
            'notification_type' => $model->notification_type,
            'context' => $model->context,
            'is_enabled' => $model->is_enabled
        ];
    }

    /**
     * @param AccountsNotifications $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(AccountsNotifications $model)
    {
        return $this->item($model->account, new AccountTransformer());
    }
}
