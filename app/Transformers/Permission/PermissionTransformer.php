<?php

namespace App\Transformers\Permission;

use League\Fractal\TransformerAbstract;
use App\Models\Permission\Permission;

/**
 * Class PermissionTransformer.
 *
 * @package namespace App\Transformers\Permission;
 */
class PermissionTransformer extends TransformerAbstract
{
    /**
     * Transform the Permission entity.
     *
     * @param \App\Models\Permission\Permission $model
     *
     * @return array
     */
    public function transform(Permission $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'slug' => $model->slug
        ];
    }
}
