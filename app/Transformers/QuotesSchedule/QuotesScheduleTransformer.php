<?php

namespace App\Transformers\QuotesSchedule;

use App\Transformers\Qoute\QuoteTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\QuotesSchedule\QuotesSchedule;

/**
 * Class QuotesScheduleTransformer.
 *
 * @package namespace App\Transformers\QuotesSchedule;
 */
class QuotesScheduleTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'quotes'
    ];

    /**
     * Transform the QuotesSchedule entity.
     *
     * @param \App\Models\QuotesSchedule\QuotesSchedule $model
     *
     * @return array
     */
    public function transform(QuotesSchedule $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'start_date' => $model->start_date,
            'end_date' => $model->end_date
        ];
    }

    /**
     * @param QuotesSchedule $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeQuotes(QuotesSchedule $model)
    {
        return $this->collection($model->quotes, new QuoteTransformer());
    }
}
