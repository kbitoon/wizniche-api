<?php

namespace App\Transformers\AccountsAddons;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Addon\AddonTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\AccountsAddons\AccountsAddons;

/**
 * Class AccountsAddonsTransformer.
 *
 * @package namespace App\Transformers\AccountsAddons;
 */
class AccountsAddonsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'addon',
        'account'
    ];

    /**
     * Transform the AccountsAddons entity.
     *
     * @param \App\Models\AccountsAddons\AccountsAddons $model
     *
     * @return array
     */
    public function transform(AccountsAddons $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'account_id' => $model->account_id,
            'addon_id' => $model->addon_id,
            'price' => $model->price,
            'added_at' => $model->added_at,
            'removed_at' => $model->removed_at
        ];
    }

    /**
     * @param AccountsAddons $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAddon(AccountsAddons $model)
    {
        return $this->item($model->addon, new AddonTransformer());
    }

    /**
     * @param AccountsAddons $model
     * @return \League\Fractal\Resource\Item
     */
    public function includeAccount(AccountsAddons $model)
    {
        return $this->item($model->account, new AccountTransformer());
    }
}
