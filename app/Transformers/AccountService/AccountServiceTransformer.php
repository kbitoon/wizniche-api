<?php

namespace App\Transformers\AccountService;

use App\Transformers\Account\AccountTransformer;
use App\Transformers\Category\CategoryTransformer;
use App\Transformers\Tag\TagTransformer;
use League\Fractal\TransformerAbstract;
use App\Models\Service\Service;

/**
 * Class ServiceTransformer.
 *
 * @package namespace App\Transformers\Service;
 */
class AccountServiceTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'tags',
        'accounts',
        'category'
    ];

    /**
     * Transform the Service entity.
     *
     * @param \App\Models\Service\Service $model
     *
     * @return array
     */
    public function transform(Service $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'description' => $model->description,
            'price' => $model->pivot->price,
            'cost' => $model->pivot->cost,
            'unit' => $model->pivot->unit,
            'is_taxable' => $model->pivot->is_taxable,
            'book_online' => $model->pivot->book_online,
            'allow_consumer_app' => $model->pivot->allow_consumer_app
        ];
    }

    /**
     * @param Service $service
     * @return \League\Fractal\Resource\Collection
     */
    public function includeTags(Service $service)
    {
        return $this->collection($service->tags, new TagTransformer());
    }

    /**
     * @param Service $service
     * @return \League\Fractal\Resource\Collection
     */
    public function includeAccounts(Service $service)
    {
        return $this->collection($service->accounts, new AccountTransformer());
    }

    /**
     * @param Service $service
     * @return \League\Fractal\Resource\Item
     */
    public function includeCategory(Service $service)
    {
        return $this->item($service->category, new CategoryTransformer());
    }
}
