<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

if (app()->getProvider('\Dingo\Api\Provider\LaravelServiceProvider')) {
    $api = app('Dingo\Api\Routing\Router');

    $api->version('v1', ['namespace' => 'App\Http\Controllers\Api'], function ($api) {

        // Authentication
        $api->post('login', 'Auth\AuthController@login');
        $api->post('register', 'Auth\AuthController@register');
        $api->post('email/verification/{id}', 'Auth\AuthController@verify');

        // Password Reset
        $api->group(['prefix' => 'password'], function ($api) {
            $api->post('create', 'PasswordResetsController@create');
            $api->get('find/{token}', 'PasswordResetsController@find');
            $api->post('reset', 'PasswordResetsController@reset');
        });

        // Quotes
        $api->get('random/quote', 'QuotesController@randomQuote');

        $api->group(['middleware' => ['auth:api', 'verified']], function ($api) {
            // Authentication
            $api->get('logout', 'Auth\AuthController@logout');
            $api->get('me', 'Auth\AuthController@authenticatedUser');

            // Permission
            $api->resource('permission', 'PermissionsController');

            // User
            $api->resource('user', 'UsersController');

            // Role
            $api->resource('role', 'RolesController');

            // Account
            $api->resource('account', 'AccountsController');

            // Employee
            $api->resource('employee', 'EmployeesController');

            // Customer
            $api->resource('customer', 'CustomersController');

            // Service
            $api->resource('service', 'ServicesController');

            // Address
            $api->resource('address', 'AddressesController');

            // Customer Type
            $api->resource('customer-type', 'CustomerTypesController');

            // Tags
            $api->resource('tag', 'TagsController');

            // Notifications
            $api->resource('notification', 'NotificationsController');

            // Payment Transactions
            $api->resource('payment-transaction', 'PaymentTransactionsController');

            // Business Hours
            $api->resource('business-hour', 'BusinessHoursController');

            // Service Areas
            $api->resource('service-area', 'ServiceAreasController');

            // Settings
            $api->resource('setting', 'SettingsController');

            // Addons
            $api->resource('addon', 'AddonsController');

            // Plans
            $api->resource('plan', 'PlansController');

            // Accounts Addons
            $api->resource('account-addon', 'AccountsAddonsController');

            // Accounts Plans
            $api->resource('account-plan', 'AccountsPlansController');

            // Accounts Notifications
            $api->resource('account-notification', 'AccountsNotificationsController');

            // Zip Codes
            $api->resource('zip-code', 'ZipCodesController');

            // Service Areas Details
            $api->resource('service-area-detail', 'ServiceAreasDetailsController');

            // Accounts Properties
            $api->resource('account-property', 'AccountsPropertiesController');

            // Employees Certifications
            $api->resource('employee-certification', 'EmployeesCertificationsController');

            // Employees Certifications
            $api->resource('employee-business-hour', 'EmployeesBusinessHoursController');

            // Positions
            $api->resource('position', 'PositionsController');

            // Quotes
            $api->resource('quote', 'QuotesController');

            // Quotes Schedules
            $api->resource('quote-schedule', 'QuotesSchedulesController');

            // Get property information by address
            $api->get('property-information', 'AccountsPropertiesController@getPropertyInformationByAddress');

            // Get all technicians by account
            $api->get('technician', 'TechniciansController@index');

            // Get all allowed roles for employees
            $api->get('employee-role', 'RolesController@getPublicRoles');
        });
    });

//    app(\Dingo\Api\Exception\Handler::class)->register(function (\Exception $exception) {
//        if (env('API_DEBUG') && !$exception instanceof \Symfony\Component\HttpKernel\Exception\HttpException) {
//            return response()->json([
//                'message'     => 'Internal server error',
//                'status_code' => 500,
//            ], 500);
//        }
//    });
}
