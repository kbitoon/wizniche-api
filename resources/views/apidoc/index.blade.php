<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>API Reference</title>

    <link rel="stylesheet" href="/docs/css/style.css" />
    <script src="/docs/js/all.js"></script>


          <script>
        $(function() {
            setupLanguages(["bash","javascript"]);
        });
      </script>
      </head>

  <body class="">
    <a href="#" id="nav-button">
      <span>
        NAV
        <img src="/docs/images/navbar.png" />
      </span>
    </a>
    <div class="tocify-wrapper">
        <img src="/docs/images/logo.png" />
                    <div class="lang-selector">
                                  <a href="#" data-language-name="bash">bash</a>
                                  <a href="#" data-language-name="javascript">javascript</a>
                            </div>
                            <div class="search">
              <input type="text" class="search" id="input-search" placeholder="Search">
            </div>
            <ul class="search-results"></ul>
              <div id="toc">
      </div>
                    <ul class="toc-footer">
                                  <li><a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a></li>
                            </ul>
            </div>
    <div class="page-wrapper">
      <div class="dark-box"></div>
      <div class="content">
          <!-- START_INFO -->
<h1>Info</h1>
<p>Welcome to the generated API reference.
<a href="{{ route("apidoc", ["format" => ".json"]) }}">Get Postman Collection</a></p>
<!-- END_INFO -->
<h1>general</h1>
<!-- START_b982a9c2785c94e078bbe534a1f12d68 -->
<h2>/api/login</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "Api-Version: v1"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "Api-Version": "v1",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST /api/login</code></p>
<!-- END_b982a9c2785c94e078bbe534a1f12d68 -->
<!-- START_0f8ecc008bbceb798251c0de85808ef8 -->
<h2>/api/register</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X POST \
    "http://localhost/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "Api-Version: v1"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "Api-Version": "v1",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<h3>HTTP Request</h3>
<p><code>POST /api/register</code></p>
<!-- END_0f8ecc008bbceb798251c0de85808ef8 -->
<!-- START_c008c148d86362a8159e47aa1b1d0409 -->
<h2>/api/logout</h2>
<blockquote>
<p>Example request:</p>
</blockquote>
<pre><code class="language-bash">curl -X GET \
    -G "http://localhost/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -H "Api-Version: v1"</code></pre>
<pre><code class="language-javascript">const url = new URL(
    "http://localhost/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
    "Api-Version": "v1",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response =&gt; response.json())
    .then(json =&gt; console.log(json));</code></pre>
<blockquote>
<p>Example response (500):</p>
</blockquote>
<pre><code class="language-json">{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate-&gt;unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate-&gt;authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline-&gt;then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router-&gt;runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router-&gt;runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router-&gt;dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router-&gt;dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel-&gt;dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router-&gt;dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request-&gt;Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline-&gt;then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request-&gt;sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request-&gt;handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline-&gt;Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline-&gt;then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel-&gt;sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel-&gt;handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls-&gt;callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls-&gt;makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls-&gt;__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator-&gt;iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator-&gt;fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator-&gt;processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation-&gt;processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation-&gt;handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container-&gt;call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command-&gt;execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command-&gt;run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command-&gt;run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application-&gt;doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application-&gt;doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application-&gt;run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application-&gt;run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche\\artisan(37): Illuminate\\Foundation\\Console\\Kernel-&gt;handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}</code></pre>
<h3>HTTP Request</h3>
<p><code>GET /api/logout</code></p>
<!-- END_c008c148d86362a8159e47aa1b1d0409 -->
      </div>
      <div class="dark-box">
                        <div class="lang-selector">
                                    <a href="#" data-language-name="bash">bash</a>
                                    <a href="#" data-language-name="javascript">javascript</a>
                              </div>
                </div>
    </div>
  </body>
</html>