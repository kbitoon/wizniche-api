---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_b982a9c2785c94e078bbe534a1f12d68 -->
## /api/login
> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/login"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/login`


<!-- END_b982a9c2785c94e078bbe534a1f12d68 -->

<!-- START_0f8ecc008bbceb798251c0de85808ef8 -->
## /api/register
> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/register"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/register`


<!-- END_0f8ecc008bbceb798251c0de85808ef8 -->

<!-- START_aacae96996577db5ab44788e7aebae24 -->
## Will create a request to reset password

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/password/create" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/password/create"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/password/create`


<!-- END_aacae96996577db5ab44788e7aebae24 -->

<!-- START_56d15ed2b61d90d41b4cc9431308547a -->
## /api/password/find/{token}
> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/password/find/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/password/find/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "error": {
        "http_code": 200,
        "message": "This password token is invalid."
    }
}
```

### HTTP Request
`GET /api/password/find/{token}`


<!-- END_56d15ed2b61d90d41b4cc9431308547a -->

<!-- START_5e9e4ac523cd01bd511cd54c969e8d9c -->
## /api/password/reset
> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/password/reset" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/password/reset"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/password/reset`


<!-- END_5e9e4ac523cd01bd511cd54c969e8d9c -->

<!-- START_c008c148d86362a8159e47aa1b1d0409 -->
## /api/logout
> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/logout"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/logout`


<!-- END_c008c148d86362a8159e47aa1b1d0409 -->

<!-- START_45b67ce8818337afa04f07656c38c0fe -->
## /api/authUser
> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/authUser" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/authUser"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/authUser`


<!-- END_45b67ce8818337afa04f07656c38c0fe -->

<!-- START_6b01185df6e52d82fad5d7daf4378ea6 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/permission`


<!-- END_6b01185df6e52d82fad5d7daf4378ea6 -->

<!-- START_7d0d6fe0f29bac82f7073916ee4726d6 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/permission" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/permission"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/permission`


<!-- END_7d0d6fe0f29bac82f7073916ee4726d6 -->

<!-- START_df309c82489b4e187adc9b79864bc195 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/permission/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/permission/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/permission/{permission}`


<!-- END_df309c82489b4e187adc9b79864bc195 -->

<!-- START_a0aeebd155073a242396e4eff2b9b569 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/permission/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/permission/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/permission/{permission}`

`PATCH /api/permission/{permission}`


<!-- END_a0aeebd155073a242396e4eff2b9b569 -->

<!-- START_19286cd3b34af4bd980a6be1d1c713f7 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/permission/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/permission/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/permission/{permission}`


<!-- END_19286cd3b34af4bd980a6be1d1c713f7 -->

<!-- START_834a4e3d053894e4b40bb4d66b99dbd4 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/user`


<!-- END_834a4e3d053894e4b40bb4d66b99dbd4 -->

<!-- START_ddfe841311822ba91babd6d2eb588875 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/user"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/user`


<!-- END_ddfe841311822ba91babd6d2eb588875 -->

<!-- START_20ab63ee1fcf8594e60ad9c2d120e13c -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/user/{user}`


<!-- END_20ab63ee1fcf8594e60ad9c2d120e13c -->

<!-- START_5a05a35f0bb24d89cffd275c59116e76 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/user/{user}`

`PATCH /api/user/{user}`


<!-- END_5a05a35f0bb24d89cffd275c59116e76 -->

<!-- START_927ee1d575721d3721514ea02524ab39 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/user/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/user/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/user/{user}`


<!-- END_927ee1d575721d3721514ea02524ab39 -->

<!-- START_bc5e1e9162385b799f2536ff7d47501f -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/role`


<!-- END_bc5e1e9162385b799f2536ff7d47501f -->

<!-- START_147daf04c8a9ea230fd0ad2a14bd08bc -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/role" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/role"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/role`


<!-- END_147daf04c8a9ea230fd0ad2a14bd08bc -->

<!-- START_b0a97ac80777a80cb3ce0f107e1eb85d -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/role/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/role/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/role/{role}`


<!-- END_b0a97ac80777a80cb3ce0f107e1eb85d -->

<!-- START_747a601455f24fe07bcaafe754c5b416 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/role/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/role/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/role/{role}`

`PATCH /api/role/{role}`


<!-- END_747a601455f24fe07bcaafe754c5b416 -->

<!-- START_2f5e9660a623e7e28f684722571f8ec0 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/role/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/role/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/role/{role}`


<!-- END_2f5e9660a623e7e28f684722571f8ec0 -->

<!-- START_ac5905d5d118885edf5996617f210f30 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/account" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/account"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/account`


<!-- END_ac5905d5d118885edf5996617f210f30 -->

<!-- START_8c074864dfde01316aeb393e7f15428f -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/account" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/account"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/account`


<!-- END_8c074864dfde01316aeb393e7f15428f -->

<!-- START_a35602b7d5a9d48aa19ff01dad296f94 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/account/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/account/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/account/{account}`


<!-- END_a35602b7d5a9d48aa19ff01dad296f94 -->

<!-- START_e17fec67dc54f2ef39d3c6750209b2ac -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/account/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/account/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/account/{account}`

`PATCH /api/account/{account}`


<!-- END_e17fec67dc54f2ef39d3c6750209b2ac -->

<!-- START_ceb449f20438720185590ccd21205cc0 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/account/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/account/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/account/{account}`


<!-- END_ceb449f20438720185590ccd21205cc0 -->

<!-- START_c30126bcdc367576b96bb1c65d4e9756 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/employee" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/employee"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/employee`


<!-- END_c30126bcdc367576b96bb1c65d4e9756 -->

<!-- START_63e82c68edc55dc7adf757f4d7f59b97 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/employee" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/employee"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/employee`


<!-- END_63e82c68edc55dc7adf757f4d7f59b97 -->

<!-- START_5f8e1c1c1ec0f7739d889d3929485a8e -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/employee/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/employee/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/employee/{employee}`


<!-- END_5f8e1c1c1ec0f7739d889d3929485a8e -->

<!-- START_ba967fec3ae1b3d81f081ed9dbae6aa6 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/employee/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/employee/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/employee/{employee}`

`PATCH /api/employee/{employee}`


<!-- END_ba967fec3ae1b3d81f081ed9dbae6aa6 -->

<!-- START_73533bd20fa7d6b1dda2d2c9837a535e -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/employee/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/employee/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/employee/{employee}`


<!-- END_73533bd20fa7d6b1dda2d2c9837a535e -->

<!-- START_a86171c876f0c0114d4c2b1a7d006fc3 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/customer" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/customer`


<!-- END_a86171c876f0c0114d4c2b1a7d006fc3 -->

<!-- START_1ecb92ad6e4730deef1421d2a54aa3c1 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/customer" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/customer`


<!-- END_1ecb92ad6e4730deef1421d2a54aa3c1 -->

<!-- START_ec82f8562d728070f1b31e0029a67b8a -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/customer/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/customer/{customer}`


<!-- END_ec82f8562d728070f1b31e0029a67b8a -->

<!-- START_5a88125d4cbec11aae568bf48f2d84aa -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/customer/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/customer/{customer}`

`PATCH /api/customer/{customer}`


<!-- END_5a88125d4cbec11aae568bf48f2d84aa -->

<!-- START_2589de39cc6641e8576991ae46b3d215 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/customer/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/customer/{customer}`


<!-- END_2589de39cc6641e8576991ae46b3d215 -->

<!-- START_3006a5adb75a57610557e1395a698b73 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/industry" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/industry"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/industry`


<!-- END_3006a5adb75a57610557e1395a698b73 -->

<!-- START_cb7f5c5eef4acaa1bae11a7f24257d2f -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/industry" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/industry"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/industry`


<!-- END_cb7f5c5eef4acaa1bae11a7f24257d2f -->

<!-- START_2b7dcaa74d14eb0828e37db99844396b -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/industry/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/industry/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/industry/{industry}`


<!-- END_2b7dcaa74d14eb0828e37db99844396b -->

<!-- START_3e1f2dc725a8f7d3e8e81e13949fcb12 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/industry/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/industry/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/industry/{industry}`

`PATCH /api/industry/{industry}`


<!-- END_3e1f2dc725a8f7d3e8e81e13949fcb12 -->

<!-- START_de5b5c7980bcc6de36ca75572a7061db -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/industry/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/industry/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/industry/{industry}`


<!-- END_de5b5c7980bcc6de36ca75572a7061db -->

<!-- START_dec2adc3e60752c72dd63d79882c4fcc -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/category`


<!-- END_dec2adc3e60752c72dd63d79882c4fcc -->

<!-- START_861dc8ea3dce5b5f3499d16a5078bcdd -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/category" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/category"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/category`


<!-- END_861dc8ea3dce5b5f3499d16a5078bcdd -->

<!-- START_2854d77126892999d400939e35a0a4d5 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/category/{category}`


<!-- END_2854d77126892999d400939e35a0a4d5 -->

<!-- START_5e7038791e43f06883d82f7bc2fac4eb -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/category/{category}`

`PATCH /api/category/{category}`


<!-- END_5e7038791e43f06883d82f7bc2fac4eb -->

<!-- START_bd65cf9616ebcdcbafc69bee7dca6551 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/category/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/category/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/category/{category}`


<!-- END_bd65cf9616ebcdcbafc69bee7dca6551 -->

<!-- START_dfeb6334ed6b5618a5e607f1bd9f2e21 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/service`


<!-- END_dfeb6334ed6b5618a5e607f1bd9f2e21 -->

<!-- START_7e77f77c55c7ba67a44fddcae85fb3f9 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/service" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/service`


<!-- END_7e77f77c55c7ba67a44fddcae85fb3f9 -->

<!-- START_6f1219da97958b09693ffce4a7de4a3a -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/service/{service}`


<!-- END_6f1219da97958b09693ffce4a7de4a3a -->

<!-- START_945b281f967b5cdb0cf05b2825d2d10c -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/service/{service}`

`PATCH /api/service/{service}`


<!-- END_945b281f967b5cdb0cf05b2825d2d10c -->

<!-- START_b8cbdc521c6b0b8648876dd26c2fcea5 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/service/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/service/{service}`


<!-- END_b8cbdc521c6b0b8648876dd26c2fcea5 -->

<!-- START_e657d687bb4bdf60adc44a350351adad -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/address" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/address"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/address`


<!-- END_e657d687bb4bdf60adc44a350351adad -->

<!-- START_e5c7bf4505e92246f0e8009c9f3c1c30 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/address" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/address"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/address`


<!-- END_e5c7bf4505e92246f0e8009c9f3c1c30 -->

<!-- START_86886d966852eb53a166751de8a1c8a7 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/address/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/address/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/address/{address}`


<!-- END_86886d966852eb53a166751de8a1c8a7 -->

<!-- START_d4251e276a6b7964c07645c1fb1e3262 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/address/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/address/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/address/{address}`

`PATCH /api/address/{address}`


<!-- END_d4251e276a6b7964c07645c1fb1e3262 -->

<!-- START_adb88974cd68ced7882e2012e30cfb01 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/address/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/address/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/address/{address}`


<!-- END_adb88974cd68ced7882e2012e30cfb01 -->

<!-- START_d8415fb63e7505a85cc00e369bf3fc6e -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/customer-type" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer-type"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/customer-type`


<!-- END_d8415fb63e7505a85cc00e369bf3fc6e -->

<!-- START_2e1f22622196702e35f7a83e63f1f5da -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/customer-type" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer-type"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/customer-type`


<!-- END_2e1f22622196702e35f7a83e63f1f5da -->

<!-- START_c02e81f47eb793eff145aab275530beb -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/customer-type/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer-type/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/customer-type/{customer_type}`


<!-- END_c02e81f47eb793eff145aab275530beb -->

<!-- START_f705eb27bf1b8b48410acf19603b28c5 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/customer-type/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer-type/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/customer-type/{customer_type}`

`PATCH /api/customer-type/{customer_type}`


<!-- END_f705eb27bf1b8b48410acf19603b28c5 -->

<!-- START_5670e617fd425b1b824cc60343f95da5 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/customer-type/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/customer-type/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/customer-type/{customer_type}`


<!-- END_5670e617fd425b1b824cc60343f95da5 -->

<!-- START_dc443e415dd8629b48c13966424a4c34 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/tag" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/tag"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/tag`


<!-- END_dc443e415dd8629b48c13966424a4c34 -->

<!-- START_da85e8a4da2d3ee83422dab7ea922869 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/tag" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/tag"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/tag`


<!-- END_da85e8a4da2d3ee83422dab7ea922869 -->

<!-- START_580de36827e91753579f57769b555415 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/tag/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/tag/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/tag/{tag}`


<!-- END_580de36827e91753579f57769b555415 -->

<!-- START_eeac026687268d6880d8f1c28e3267cf -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/tag/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/tag/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/tag/{tag}`

`PATCH /api/tag/{tag}`


<!-- END_eeac026687268d6880d8f1c28e3267cf -->

<!-- START_fbf4f2db279097d12c29e8a9555707b5 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/tag/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/tag/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/tag/{tag}`


<!-- END_fbf4f2db279097d12c29e8a9555707b5 -->

<!-- START_e189ba00ca66e46d937528ff1bfb9854 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/notification" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/notification"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/notification`


<!-- END_e189ba00ca66e46d937528ff1bfb9854 -->

<!-- START_900a3485c7bb0bc634cf2df118149648 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/notification" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/notification"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/notification`


<!-- END_900a3485c7bb0bc634cf2df118149648 -->

<!-- START_c4bb1a14067317ac08b9c6006567540d -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/notification/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/notification/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/notification/{notification}`


<!-- END_c4bb1a14067317ac08b9c6006567540d -->

<!-- START_6a0f0ca378bbe3553b7c236f31e6f493 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/notification/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/notification/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/notification/{notification}`

`PATCH /api/notification/{notification}`


<!-- END_6a0f0ca378bbe3553b7c236f31e6f493 -->

<!-- START_899a25e06312c020f2b9ac63609f9f9a -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/notification/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/notification/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/notification/{notification}`


<!-- END_899a25e06312c020f2b9ac63609f9f9a -->

<!-- START_b21210d86bd3975b27b737c7aea5f875 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/payment-transaction" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/payment-transaction"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/payment-transaction`


<!-- END_b21210d86bd3975b27b737c7aea5f875 -->

<!-- START_9370ce689669cc98ec89f0cb61123a98 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/payment-transaction" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/payment-transaction"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/payment-transaction`


<!-- END_9370ce689669cc98ec89f0cb61123a98 -->

<!-- START_87014764ab7f64dcf96dcea2d591a9d6 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/payment-transaction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/payment-transaction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/payment-transaction/{payment_transaction}`


<!-- END_87014764ab7f64dcf96dcea2d591a9d6 -->

<!-- START_9e5516023db79847eb1fa0cb0431f6bd -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/payment-transaction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/payment-transaction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/payment-transaction/{payment_transaction}`

`PATCH /api/payment-transaction/{payment_transaction}`


<!-- END_9e5516023db79847eb1fa0cb0431f6bd -->

<!-- START_c22994f8a2392d7e2abe382123863aed -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/payment-transaction/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/payment-transaction/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/payment-transaction/{payment_transaction}`


<!-- END_c22994f8a2392d7e2abe382123863aed -->

<!-- START_b61c1daaf18dda84e4f3ffe38733a148 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/business-hour" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/business-hour"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/business-hour`


<!-- END_b61c1daaf18dda84e4f3ffe38733a148 -->

<!-- START_f63c1092238367247b132f3e3b8a3c09 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/business-hour" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/business-hour"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/business-hour`


<!-- END_f63c1092238367247b132f3e3b8a3c09 -->

<!-- START_fe9b26ab7f509bfff54ea93985736e3e -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/business-hour/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/business-hour/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/business-hour/{business_hour}`


<!-- END_fe9b26ab7f509bfff54ea93985736e3e -->

<!-- START_67f34c9bb796db517b73f40e0e4421f8 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/business-hour/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/business-hour/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/business-hour/{business_hour}`

`PATCH /api/business-hour/{business_hour}`


<!-- END_67f34c9bb796db517b73f40e0e4421f8 -->

<!-- START_cb9a5bc201735dc2c055e80d20d7234f -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/business-hour/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/business-hour/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/business-hour/{business_hour}`


<!-- END_cb9a5bc201735dc2c055e80d20d7234f -->

<!-- START_55164dc6420f0e3f1dc468ec0535a3b3 -->
## Display a listing of the resource.

GET /api/{resource}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/service-area" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service-area"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/service-area`


<!-- END_55164dc6420f0e3f1dc468ec0535a3b3 -->

<!-- START_ee5297f514084c949b6ce11b29e01530 -->
## Store a newly created resource in storage.

POST /api/{resource}.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/service-area" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service-area"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST /api/service-area`


<!-- END_ee5297f514084c949b6ce11b29e01530 -->

<!-- START_f0b469659b9a5858d0dbb43e59ef7e07 -->
## Display the specified resource.

GET /api/{resource}/{id}.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/service-area/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service-area/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "message": "Unauthenticated.",
    "status_code": 500,
    "debug": {
        "line": 81,
        "file": "D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php",
        "class": "Illuminate\\Auth\\AuthenticationException",
        "trace": [
            "#0 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(67): Illuminate\\Auth\\Middleware\\Authenticate->unauthenticated(Object(Dingo\\Api\\Http\\Request), Array)",
            "#1 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Auth\\Middleware\\Authenticate.php(41): Illuminate\\Auth\\Middleware\\Authenticate->authenticate(Object(Dingo\\Api\\Http\\Request), Array)",
            "#2 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Auth\\Middleware\\Authenticate->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure), 'api')",
            "#3 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\PrepareController.php(45): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#4 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\PrepareController->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#5 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#6 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(683): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#7 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(658): Illuminate\\Routing\\Router->runRouteWithinStack(Object(Illuminate\\Routing\\Route), Object(Dingo\\Api\\Http\\Request))",
            "#8 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(624): Illuminate\\Routing\\Router->runRoute(Object(Dingo\\Api\\Http\\Request), Object(Illuminate\\Routing\\Route))",
            "#9 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php(613): Illuminate\\Routing\\Router->dispatchToRoute(Object(Dingo\\Api\\Http\\Request))",
            "#10 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Adapter\\Laravel.php(88): Illuminate\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#11 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Routing\\Router.php(518): Dingo\\Api\\Routing\\Adapter\\Laravel->dispatch(Object(Dingo\\Api\\Http\\Request), 'v1')",
            "#12 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(126): Dingo\\Api\\Routing\\Router->dispatch(Object(Dingo\\Api\\Http\\Request))",
            "#13 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(130): Dingo\\Api\\Http\\Middleware\\Request->Dingo\\Api\\Http\\Middleware\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#14 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#15 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#16 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php(21): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#17 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#18 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php(27): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#19 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#20 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php(63): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#21 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#22 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\fideloper\\proxy\\src\\TrustProxies.php(57): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#23 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Fideloper\\Proxy\\TrustProxies->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#24 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Dingo\\Api\\Http\\Request))",
            "#25 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(127): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#26 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\dingo\\api\\src\\Http\\Middleware\\Request.php(103): Dingo\\Api\\Http\\Middleware\\Request->sendRequestThroughRouter(Object(Dingo\\Api\\Http\\Request))",
            "#27 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(171): Dingo\\Api\\Http\\Middleware\\Request->handle(Object(Dingo\\Api\\Http\\Request), Object(Closure))",
            "#28 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Http\\Request))",
            "#29 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(145): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))",
            "#30 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php(110): Illuminate\\Foundation\\Http\\Kernel->sendRequestThroughRouter(Object(Illuminate\\Http\\Request))",
            "#31 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(307): Illuminate\\Foundation\\Http\\Kernel->handle(Object(Illuminate\\Http\\Request))",
            "#32 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(289): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->callLaravelRoute(Object(Illuminate\\Http\\Request))",
            "#33 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Strategies\\Responses\\ResponseCalls.php(47): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->makeApiCall(Object(Illuminate\\Http\\Request))",
            "#34 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(170): Mpociot\\ApiDoc\\Extracting\\Strategies\\Responses\\ResponseCalls->__invoke(Object(Dingo\\Api\\Routing\\Route), Object(ReflectionClass), Object(ReflectionMethod), Array, Array)",
            "#35 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(119): Mpociot\\ApiDoc\\Extracting\\Generator->iterateThroughStrategies('responses', Array, Array)",
            "#36 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Extracting\\Generator.php(82): Mpociot\\ApiDoc\\Extracting\\Generator->fetchResponses(Object(ReflectionClass), Object(ReflectionMethod), Object(Dingo\\Api\\Routing\\Route), Array, Array)",
            "#37 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(117): Mpociot\\ApiDoc\\Extracting\\Generator->processRoute(Object(Dingo\\Api\\Routing\\Route), Array)",
            "#38 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php(79): Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->processRoutes(Object(Mpociot\\ApiDoc\\Extracting\\Generator), Array)",
            "#39 [internal function]: Mpociot\\ApiDoc\\Commands\\GenerateDocumentation->handle()",
            "#40 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(32): call_user_func_array(Array, Array)",
            "#41 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()",
            "#42 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(90): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))",
            "#43 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(34): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))",
            "#44 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)",
            "#45 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(201): Illuminate\\Container\\Container->call(Array)",
            "#46 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Command\\Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#47 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(188): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))",
            "#48 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(1011): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#49 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(272): Symfony\\Component\\Console\\Application->doRunCommand(Object(Mpociot\\ApiDoc\\Commands\\GenerateDocumentation), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#50 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\symfony\\console\\Application.php(148): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#51 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#52 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#53 D:\\Development\\Fullscale\\htdocs\\wizniche-api\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))",
            "#54 {main}"
        ]
    }
}
```

### HTTP Request
`GET /api/service-area/{service_area}`


<!-- END_f0b469659b9a5858d0dbb43e59ef7e07 -->

<!-- START_2ebfc01e7aac789fe696f18f7f35a1a0 -->
## Update the specified resource in storage.

PUT /api/{resource}/{id}.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/service-area/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service-area/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT /api/service-area/{service_area}`

`PATCH /api/service-area/{service_area}`


<!-- END_2ebfc01e7aac789fe696f18f7f35a1a0 -->

<!-- START_8403a4a849cbfb21bdfae6a0924ad845 -->
## Remove the specified resource from storage.

DELETE /api/{resource}/{id}.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/service-area/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/service-area/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE /api/service-area/{service_area}`


<!-- END_8403a4a849cbfb21bdfae6a0924ad845 -->


